# Competispy-frontend

## Testing locally

We support running the entire project locally by simply running

```shell
npm start
```

This will bring up the ui with some basic data in it, and you will get live reloading

## Testing in a cluster

Alternatively you can also leverage skaffold to do the same thing.
This has the benefit of offloading the running of the server to the cluster if
you are running in a constrained system.

Simply start the dev loop of skaffold
```shell
skaffold dev
```

It is auto-detect that you want the dev profile and will build the development docker image.
This is slightly different from the production one to allow for quick reloading of the project
thanks to the [file sync](https://skaffold.dev/docs/pipeline-stages/filesync/) functionality.

> ⚠ in the future we may want to also give a semi quick option closer to production, ie running a
> nginx instance but with a slower turn around time. This is not a priority for now since you can 
> run the development docker image and the just quickly check once if you nginx config is correct, 
> and if it isn't then simply changing the config won't necessitate the re-compile of all files
> and will use the docker cache instead keeping the process relatively fast.

This is also useful if you are trying to test fault scenarios in the cluster such as the db being restarted
or a sudden death of a node in the cluster.

As a last nore you can also combine this method with [telepresence](https://www.telepresence.io/) 
and run the project again locally