FROM emscripten/emsdk:3.1.55 as wasm-builder
RUN wget https://github.com/Kitware/CMake/releases/download/v3.29.0-rc3/cmake-3.29.0-rc3-linux-x86_64.sh && chmod +x cmake-3.29.0-rc3-linux-x86_64.sh && ./cmake-3.29.0-rc3-linux-x86_64.sh --skip-license --exclude-subdir --prefix=/usr && wget https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-linux.zip && unzip ninja-linux.zip && mv ninja /usr/bin/
WORKDIR /app
COPY CMakeLists.txt CMakePresets.json ./
COPY wasmlib ./wasmlib/
RUN mkdir /app/src && cmake --preset emscripten-release && cmake --build cmake-build-emscripten-release

FROM node:20.11.1-alpine3.18 as development
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY tsconfig.json craco.config.js .eslintignore .eslintrc.json ./
COPY public public/
COPY src src/
COPY --from=wasm-builder /app/src/* /app/src/
ENV PORT=3000
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["npm","run", "start"]

FROM development as build-deps
RUN npm run build

FROM nginx:1.17.10-alpine
COPY --from=build-deps /app/build /usr/share/nginx/html
RUN rm -rf /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d
EXPOSE 80

