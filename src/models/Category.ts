import { range } from "../values";

export const categories = range(20, 100, 5);
export type relayCategoriesT = 80 | 100 | 120 | 160 | 200 | 240 | 280 | 320;
export const relayCategories = [80, 100, 120, 160, 200, 240, 280, 320];
