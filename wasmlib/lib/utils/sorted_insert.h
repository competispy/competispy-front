#ifndef COMPETYSPY_FRONT_SORTED_INSERT_H
#define COMPETYSPY_FRONT_SORTED_INSERT_H

#include <algorithm>
#include <functional>
#include <vector>

template<typename T>
auto insert_sorted(std::vector<T>& vec, T&& item,
                   std::function<bool(const T& a, const T& b)> compare = std::less<T>()) ->
    typename std::vector<T>::iterator {
  return vec.insert(std::upper_bound(vec.begin(), vec.end(), item, compare), std::move(item));
}

#endif  // COMPETYSPY_FRONT_SORTED_INSERT_H
