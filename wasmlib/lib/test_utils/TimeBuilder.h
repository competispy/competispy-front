#ifndef COMPETYSPY_FRONT_TIME_BUILDER_H
#define COMPETYSPY_FRONT_TIME_BUILDER_H

class TimeBuilder {
public:
  TimeBuilder() = default;

  auto hours(float h) -> TimeBuilder& { return minutes(h * 60); }

  auto minutes(float m) -> TimeBuilder& { return seconds(m * 60); }

  auto seconds(float s) -> TimeBuilder& {
    time_ += s;
    return *this;
  }

  auto cents(float c) -> TimeBuilder& { return seconds(c / 100); }

  auto build() -> float { return time_; }

private:
  float time_ = 0;
};

#endif
