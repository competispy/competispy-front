#ifndef COMPETYSPY_FRONT_STYLETIMES_H
#define COMPETYSPY_FRONT_STYLETIMES_H

#include <cmath>
#include <cstdint>
#include <iostream>
#include <limits>
#include <unordered_map>

#include "nlohmann/json_fwd.hpp"

#include "Event.h"
#include "Style.h"

class StyleTimes {
public:
  explicit StyleTimes(Style style) : style_(style) {}

  [[nodiscard]] auto getTimeForDistance(uint16_t distance) const -> float {
    const auto tIt = times_.find(distance);
    return tIt != times_.end() ? tIt->second : NO_TIME;
  }

  void setTimeForDistance(uint16_t distance, float time) {
    if (isDistanceValid(distance)) { times_[distance] = time; }
  }

  [[nodiscard]] auto getStyle() const -> Style { return style_; }

  [[nodiscard]] auto isDistanceValid(uint16_t distance) const -> bool {
    switch (style_) {
      case Style::BUTTERFLY :
      case Style::BACK :
      case Style::BREAST :
        return distance == 50 || distance == 100 || distance == 200;
      case Style::FREE :
        return distance == 50 || distance == 100 || distance == 200 || distance == 400
            || distance == 800 || distance == 1500 || distance == 3000;
      case Style::MEDLEY :
        return distance == 100 || distance == 200 || distance == 400;
    }
    return false;
  }

  static auto timesEqual(float a, float b) -> bool { return std::fabs(a - b) < 0.01; }

  static auto isNoTime(float time) -> bool { return timesEqual(time, NO_TIME); }

  static void to_json(nlohmann::json& j, const StyleTimes& styleTimes) {
    j["style"] = styleTimes.getStyle();
    j["times"] = styleTimes.times_;
  }

  static auto from_json(const nlohmann::json& j) -> StyleTimes {
    StyleTimes styleTimes(j.at("style").get<Style>());
    j.at("times").get_to(styleTimes.times_);
    return styleTimes;
  }

  constexpr static const float NO_TIME = std::numeric_limits<float>::max();

private:
  Style                               style_;
  std::unordered_map<uint16_t, float> times_;
};

namespace nlohmann {
template<>
struct adl_serializer<StyleTimes> {
  static auto from_json(const json& j) -> StyleTimes { return StyleTimes::from_json(j); }

  static void to_json(json& j, const StyleTimes& t) { StyleTimes::to_json(j, t); }
};
}  // namespace nlohmann

#endif  // COMPETYSPY_FRONT_STYLETIMES_H
