add_library(models OBJECT)
target_sources(models
    PRIVATE
        Event.h
        Gender.h
        Relay.h
        RelayCategory.h
        Session.h
        Style.h
        StyleTimes.h
        Swimmer.h
        SwimmerEventCollection.h
        ConstSwimmerIterator.h
        SwimmerObserver.h
        SwimmerProvider.h
        SwimmerProxy.h
        SwimmersCollection.h
        Team.h
        Times.h
)
target_include_directories(models PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(models PUBLIC nlohmann_json::nlohmann_json spdlog::spdlog PRIVATE utils)
set_target_properties(models PROPERTIES LINKER_LANGUAGE CXX)

add_subdirectory(local)

