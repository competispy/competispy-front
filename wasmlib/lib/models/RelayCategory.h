#ifndef COMPETYSPY_FRONT_RELAYCATEGORY_H
#define COMPETYSPY_FRONT_RELAYCATEGORY_H

#include <cstdint>
#include <nlohmann/json.hpp>
#include <tuple>

enum class RelayCategory {
  _80,
  _100,
  _120,
  _160,
  _200,
  _240,
  _280,
  _320,
  _360,
};

using RelayCategoryAgeRange = std::tuple<uint16_t, uint16_t>;

constexpr auto getRelayCategoryRange(RelayCategory category) -> RelayCategoryAgeRange {
  switch (category) {
    case RelayCategory::_80 :
      return {80, 99};
    case RelayCategory::_100 :
      return {100, 119};
    case RelayCategory::_120 :
      return {120, 159};
    case RelayCategory::_160 :
      return {160, 199};
    case RelayCategory::_200 :
      return {200, 239};
    case RelayCategory::_240 :
      return {240, 279};
    case RelayCategory::_280 :
      return {280, 319};
    case RelayCategory::_320 :
      return {320, 359};
    case RelayCategory::_360 :
      return {360, 399};
  }
  return {};
}

NLOHMANN_JSON_SERIALIZE_ENUM(RelayCategory, {
                                              { RelayCategory::_80,  "+80"},
                                              {RelayCategory::_100, "+100"},
                                              {RelayCategory::_120, "+120"},
                                              {RelayCategory::_160, "+160"},
                                              {RelayCategory::_200, "+200"},
                                              {RelayCategory::_240, "+240"},
                                              {RelayCategory::_280, "+280"},
                                              {RelayCategory::_320, "+320"},
                                              {RelayCategory::_360, "+360"},
})

#endif  // COMPETYSPY_FRONT_RELAYCATEGORY_H
