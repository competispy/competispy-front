#ifndef COMPETYSPY_FRONT_CONSTSWIMMERITERATOR_H
#define COMPETYSPY_FRONT_CONSTSWIMMERITERATOR_H

#include "Swimmer.h"

class ConstSwimmerIterator {
public:
  virtual ~ConstSwimmerIterator()                        = default;
  virtual auto               getNext() -> const Swimmer& = 0;
  [[nodiscard]] virtual auto hasMore() const -> bool     = 0;

  /*
  using pointer           = const Swimmer*;
  using reference         = const Swimmer&;

  virtual reference operator*() const = 0;
  virtual pointer operator->() = 0;
  virtual ConstSwimmerIterator& operator++() = 0;

  friend bool operator== (const ConstSwimmerIterator& a, const ConstSwimmerIterator& b) { return *a
  == *b; }; friend bool operator!= (const ConstSwimmerIterator& a, const ConstSwimmerIterator& b) {
  return !(a == b); };
   */
};

#endif  // COMPETYSPY_FRONT_CONSTSWIMMERITERATOR_H
