#ifndef COMPETYSPY_FRONT_SWIMMER_PROVIDER_H
#define COMPETYSPY_FRONT_SWIMMER_PROVIDER_H

#include "ConstSwimmerIterator.h"
#include "Swimmer.h"

class SwimmerProvider {
public:
  virtual ~SwimmerProvider() = default;
  [[nodiscard]] virtual auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& = 0;
  [[nodiscard]] virtual auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer&             = 0;

  [[nodiscard]] virtual auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator>
                                                           = 0;
};

#endif
