#ifndef COMPETYSPY_FRONT_TEAM_LOCAL_H
#define COMPETYSPY_FRONT_TEAM_LOCAL_H

#include <gsl/pointers>
#include <unordered_map>
#include <unordered_set>

#include "nlohmann/json.hpp"

#include "SessionLocal.h"
#include "Swimmer.h"
#include "SwimmerProvider.h"
#include "Team.h"

class TeamLocal : public Team {
  using json               = nlohmann::json;
  using SwimmersCollection = std::unordered_set<::Swimmer::SwimmerId, Swimmer::SwimmerId::Hash>;

  class SwimmersCollectionIterator : public ConstSwimmerIterator {
  public:
    explicit SwimmersCollectionIterator(const SwimmerProvider&    swimmerProvider,
                                        const SwimmersCollection& collection) :
        swimmerProvider_(&swimmerProvider),
        it_(collection.begin()),
        end_(collection.end()) {}

    auto getNext() -> const Swimmer& override {
      const Swimmer& swimmer = swimmerProvider_->getSwimmer(*it_);
      if (hasMore()) { ++it_; }
      return swimmer;
    }

    [[nodiscard]] auto hasMore() const -> bool override { return it_ != end_; }

  private:
    gsl::not_null<const SwimmerProvider*> swimmerProvider_;
    SwimmersCollection::const_iterator    it_;
    SwimmersCollection::const_iterator    end_;
  };

public:
  explicit TeamLocal(SwimmerProvider& swimmerProvider);
  TeamLocal(const json& jsonValue, SwimmerProvider& swimmerProvider);
  ~TeamLocal() override = default;
  void               addSession(const std::string& sessionId) override;
  void               removeSession(const std::string& sessionId) override;
  auto               getSession(const std::string& sessionId) -> Session& override;
  [[nodiscard]] auto getSession(const std::string& sessionId) const -> const Session& override;
  [[nodiscard]] auto hasSession(const std::string& sessionId) const -> bool override;
  [[nodiscard]] auto getSessionIds() const -> std::vector<std::string> override;

  void               addSwimmer(const Swimmer::SwimmerId& swimmerId) override;
  void               removeSwimmer(const Swimmer::SwimmerId& swimmerId) override;
  [[nodiscard]] auto hasSwimmer(const Swimmer::SwimmerId& swimmerId) const -> bool override;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& swimmerId) const
      -> const Swimmer& override;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& swimmerId) -> Swimmer& override;

  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override {
    return std::make_unique<SwimmersCollectionIterator>(*swimmerProvider_, swimmers_);
  }

  [[nodiscard]] auto getSwimmersSize() const -> size_t override { return swimmers_.size(); }

  operator json() const;

private:
  gsl::not_null<SwimmerProvider*>               swimmerProvider_;
  std::unordered_map<std::string, SessionLocal> sessions_;
  SwimmersCollection                            swimmers_;
};

#endif
