#include "SwimmerLocal.h"

#include "Gender.h"
#include "Times.h"

using json = nlohmann::json;

SwimmerLocal::SwimmerLocal(std::string name, std::string surname, uint16_t bornYear,
                           Gender gender) :
    id_(std::move(name), std::move(surname), bornYear, gender) {
  spdlog::debug("{} created", *this);
}

SwimmerLocal::SwimmerLocal(Swimmer::SwimmerId swimmerId) : id_(std::move(swimmerId)) {
  spdlog::debug("{} created", *this);
}

SwimmerLocal::SwimmerLocal(SwimmerLocal&& swimmer) noexcept :
    Swimmer(std::move(swimmer)),
    id_(std::move(swimmer.id_)),
    times_(std::move(swimmer.times_)) {
  spdlog::debug("{} move created", *this);
}

SwimmerLocal::~SwimmerLocal() {
  spdlog::debug("{} destroying", *this);
  notifyDeletion();
  spdlog::debug("{} destroyed", *this);
}

[[nodiscard]] auto SwimmerLocal::getTime(Event event, Times::Pool pool) const -> float {
  return id_.gender == event.gender ? times_.getTime(event, pool) : StyleTimes::NO_TIME;
}

[[nodiscard]] auto SwimmerLocal::getTimeAutoConvert(Event event, Times::Pool pool) const -> float {
  return id_.gender == event.gender ? times_.getTimeAutoConvert(event, pool) : StyleTimes::NO_TIME;
}

[[nodiscard]] auto SwimmerLocal::getAge(uint16_t actualYear) const -> uint16_t {
  return actualYear - id_.bornYear;
}

[[nodiscard]] auto SwimmerLocal::getName() const -> const std::string& { return id_.name; }

[[nodiscard]] auto SwimmerLocal::getSurname() const -> const std::string& { return id_.surname; }

[[nodiscard]] auto SwimmerLocal::getBornYear() const -> uint16_t { return id_.bornYear; }

[[nodiscard]] auto SwimmerLocal::getGender() const -> Gender { return id_.gender; }

void               SwimmerLocal::setEventTime(const Event& event, float time, Times::Pool pool) {
  times_.setEventTime(event, time, pool);
  notifyUpdated();
}

auto SwimmerLocal::operator==(const SwimmerLocal& rhs) const -> bool {
  return getId() == rhs.getId();
}

[[nodiscard]] auto SwimmerLocal::getId() const -> const Swimmer::SwimmerId& { return id_; }

SwimmerLocal::operator json() const {
  json jsonValue;
  jsonValue["id"]    = id_;
  jsonValue["times"] = times_;
  return jsonValue;
}

SwimmerLocal::SwimmerLocal(const nlohmann::json& jsonValue) :
    SwimmerLocal(jsonValue.at("id").get<Swimmer::SwimmerId>()) {
  jsonValue.at("times").get_to(times_);
}
