#include "RelayLocal.h"

#include <cstddef>

#include "spdlog/spdlog.h"

#include "Event.h"
#include "Gender.h"
#include "RelayCategory.h"
#include "Swimmer.h"
#include "SwimmerLocal.h"

using json = nlohmann::json;

RelayLocal::RelayLocal(Event event, RelayCategory category, Times::Pool pool) :
    RelayLocal(Relay::Id{event, category, pool}) {}

RelayLocal::RelayLocal(Relay::Id relayId) :
    relayId_(relayId),
    styles_(getStyleArray(relayId.event.style)) {
  spdlog::debug("{} created", *this);
}

RelayLocal::RelayLocal(
    const nlohmann::json&                                                        j,
    const std::function<std::unique_ptr<Swimmer>(const Swimmer::SwimmerId& id)>& getSwimmer) :
    RelayLocal(j.at("event").get<Event>(), j.at("category").get<RelayCategory>()) {
  uint8_t position = 0;
  for (const auto& swimmerIdJson : j["swimmers"]) {
    if (swimmerIdJson.is_null()) {
      ++position;
      continue;
    }
    const auto swimmerId = swimmerIdJson.get<Swimmer::SwimmerId>();
    Relay::setSwimmer(getSwimmer(swimmerId), position++);
  }
}

RelayLocal::~RelayLocal() {
  spdlog::debug("{} destroying", *this);
  for (size_t i = 0; i < swimmers_.size(); ++i) { unregisterSwimmerDestroyObserver(i); }
  for (auto* observers : relayChangedObservers_) { observers->notifyRemoved(); }
  spdlog::debug("{} destroyed", *this);
}

void RelayLocal::setSwimmer(std::unique_ptr<Swimmer> swimmer, uint8_t position,
                            const std::function<void(const Relay*)>& callback) {
  if (position >= swimmers_.size()) { return; }
  {
    Relay::RelayNotificationTemporalDisabler notiifcationDisabler(*this);
    removeSwimmer(position);
    if (swimmer) { swimmer->registerSwimmerObserver(*this); }
    swimmers_[position] = std::move(swimmer);
    if (callback && isFull()) { callback(this); }
  }
  notifyRelayChanged();
}

[[nodiscard]] auto RelayLocal::getId() const -> Relay::Id { return relayId_; }

[[nodiscard]] auto RelayLocal::getEvent() const -> Event { return relayId_.event; }

[[nodiscard]] auto RelayLocal::getCategory() const -> RelayCategory { return relayId_.category; }

auto               RelayLocal::removeSwimmer(uint8_t position) -> std::unique_ptr<Swimmer> {
  unregisterSwimmerDestroyObserver(position);
  auto swimmer = std::move(swimmers_[position]);
  if (swimmer != nullptr) { notifyRelayChanged(); }
  return std::move(swimmer);
}

void RelayLocal::clean() {
  bool relayChanged = false;
  {
    Relay::RelayNotificationTemporalDisabler notiifcationDisabler(*this);
    for (size_t i = 0; i < swimmers_.size(); ++i) { relayChanged |= removeSwimmer(i) != nullptr; }
  }
  if (relayChanged) { notifyRelayChanged(); }
}

[[nodiscard]] auto RelayLocal::isFull() const -> bool {
  for (const auto& sw : swimmers_) {
    if (nullptr == sw) { return false; }
  }
  return true;
}

void RelayLocal::visitSwimmers(std::function<void(const Swimmer* swimmer, size_t index)> fn) {
  for (size_t i = 0; i < swimmers_.size(); ++i) { fn(swimmers_[i].get(), i); }
}

void RelayLocal::visitSwimmers(std::function<void(const Swimmer* swimmer, size_t index)> fn) const {
  for (size_t i = 0; i < swimmers_.size(); ++i) { fn(swimmers_[i].get(), i); }
}

auto RelayLocal::getEventForPosition(size_t position) const -> Event {
  return {styles_[position], relayId_.event.distance, relayId_.event.gender};
}

auto RelayLocal::getPool() const -> Times::Pool { return relayId_.pool; }

RelayLocal::operator json() {
  nlohmann::json j;
  j["event"]    = getEvent();
  j["category"] = getCategory();
  visitSwimmers([&j](const Swimmer* swimmer, size_t index) {
    if (swimmer) {
      j["swimmers"].emplace_back(swimmer->getId());
    } else {
      j["swimmers"].emplace_back(nullptr);
    }
  });
  return j;
}

void RelayLocal::notifyDeleted(const Swimmer& swimmer) {
  if (const auto& it = std::find_if(
          swimmers_.begin(), swimmers_.end(),
          [&swimmer](const std::unique_ptr<Swimmer>& s) { return s != nullptr && *s == swimmer; });
      it != swimmers_.end()) {
    *it = nullptr;
    notifyRelayChanged();
  }
}

void RelayLocal::notifyUpdated(const Swimmer& swimmer) {
  if (const auto& it = std::find_if(
          swimmers_.begin(), swimmers_.end(),
          [&swimmer](const std::unique_ptr<Swimmer>& s) { return s != nullptr && *s == swimmer; });
      it != swimmers_.end()) {
    notifyRelayChanged();
  }
}

void RelayLocal::unregisterSwimmerDestroyObserver(uint8_t position) {
  if (nullptr != swimmers_[position]) { swimmers_[position]->unregisterSwimmerObserver(*this); }
}

void RelayLocal::registerRelayObserver(RelayObserver* relayObserver) {
  relayChangedObservers_.emplace(relayObserver);
}

void RelayLocal::unregisterRelayObserver(RelayObserver* relayObserver) {
  relayChangedObservers_.erase(relayObserver);
}

void RelayLocal::notifyRelayChanged() const {
  spdlog::debug("{} notifying observers {} is enabled {}", *this, relayId_,
                isChangeNotificationEnabled());
  if (not isChangeNotificationEnabled()) { return; }
  for (auto* relayObserver : relayChangedObservers_) { relayObserver->notifyChanged(); }
}
