#include "SwimmersCollectionLocal.h"

#include "SwimmerLocal.h"

void SwimmersCollectionLocal::addSwimmer(Swimmer::SwimmerId id) {
  swimmers_.try_emplace(std::move(id), std::make_unique<SwimmerLocal>(id));
}

void SwimmersCollectionLocal::addSwimmer(std::string name, std::string surname, uint16_t bornYear,
                                         Gender gender) {
  addSwimmer({std::move(name), std::move(surname), bornYear, gender});
}

void SwimmersCollectionLocal::removeSwimmer(const Swimmer::SwimmerId& id) { swimmers_.erase(id); }

void SwimmersCollectionLocal::removeSwimmer(std::string name, std::string surname,
                                            uint16_t bornYear, Gender gender) {
  removeSwimmer({std::move(name), std::move(surname), bornYear, gender});
}

auto SwimmersCollectionLocal::hasSwimmer(const Swimmer::SwimmerId& swimmerId) const -> bool {
  return swimmers_.contains(swimmerId);
}

void               SwimmersCollectionLocal::cleanAllSwimmers() { swimmers_.clear(); }

[[nodiscard]] auto SwimmersCollectionLocal::getSwimmer(const Swimmer::SwimmerId& id) const
    -> const Swimmer& {
  return *(swimmers_.at(id));
}

[[nodiscard]] auto SwimmersCollectionLocal::getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& {
  return *(swimmers_.at(id));
}
