#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Event.h"
#include "Gender.h"
#include "Style.h"
#include "Swimmer.h"
#include "SwimmerLocal.h"
#include "SwimmerLocalBuilder.h"
#include "Times.h"

TEST(SwimmerLocal, GIVEN_2_swimmers_with_same_all_WHEN_check_eq_THEN_is_true) {
  const auto     swl1 = SwimmerLocalBuilder().build();
  const auto     swl2 = SwimmerLocalBuilder().build();
  const Swimmer& sw1  = swl1;
  const Swimmer& sw2  = swl2;
  EXPECT_EQ(swl1, swl2);
  EXPECT_EQ(sw1, sw2);
}

TEST(SwimmerLocal, GIVEN_2_swimmers_with_different_name_WHEN_check_eq_THEN_is_false) {
  const auto     swl1 = SwimmerLocalBuilder().name("n1").build();
  const auto     swl2 = SwimmerLocalBuilder().name("n2").build();
  const Swimmer& sw1  = swl1;
  const Swimmer& sw2  = swl2;
  EXPECT_NE(swl1, swl2);
  EXPECT_NE(sw1, sw2);
}

TEST(SwimmerLocal, GIVEN_2_swimmers_with_different_surname_WHEN_check_eq_THEN_is_false) {
  const auto     swl1 = SwimmerLocalBuilder().surname("n1").build();
  const auto     swl2 = SwimmerLocalBuilder().surname("n2").build();
  const Swimmer& sw1  = swl1;
  const Swimmer& sw2  = swl2;
  EXPECT_NE(swl1, swl2);
  EXPECT_NE(sw1, sw2);
}

TEST(SwimmerLocal, GIVEN_2_swimmers_with_different_bornYear_WHEN_check_eq_THEN_is_false) {
  const auto     swl1 = SwimmerLocalBuilder().bornYear(1980).build();
  const auto     swl2 = SwimmerLocalBuilder().bornYear(1981).build();
  const Swimmer& sw1  = swl1;
  const Swimmer& sw2  = swl2;
  EXPECT_NE(swl1, swl2);
  EXPECT_NE(sw1, sw2);
}

TEST(SwimmerLocal, GIVEN_2_swimmers_with_different_gender_WHEN_check_eq_THEN_is_true) {
  const auto     swl1 = SwimmerLocalBuilder().gender(Gender::MALE).build();
  const auto     swl2 = SwimmerLocalBuilder().gender(Gender::FEMALE).build();
  const Swimmer& sw1  = swl1;
  const Swimmer& sw2  = swl2;
  EXPECT_EQ(swl1, swl2);
  EXPECT_EQ(sw1, sw2);
}

TEST(SwimmerLocal, GIVEN_a_swimmer_generated_json_dump_WHEN_recover_THEN_is_eq_to_original) {
  const auto     swl1      = SwimmerLocalBuilder().build();
  nlohmann::json swl1_json = swl1;
  auto           swl2      = swl1_json.get<SwimmerLocal>();
  const Swimmer& sw1       = swl1;
  const Swimmer& sw2       = swl2;
  EXPECT_EQ(swl1, swl2);
  EXPECT_EQ(sw1, sw2);
}

TEST(SwimmerLocal,
     GIVEN_a_swimmers_with_time_in_50_free_short_1_second_WHEN_get_this_time_THEN_1_is_obtained) {
  auto        swimmer = SwimmerLocalBuilder().build();
  const Event event{Style::FREE, 50, EventGender::MALE};
  swimmer.setEventTime(event, 1, Times::_25);
  EXPECT_EQ(swimmer.getTime(event, Times::_25), 1);
}
