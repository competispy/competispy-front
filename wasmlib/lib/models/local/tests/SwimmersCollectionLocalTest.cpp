#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "Event.h"
#include "Style.h"
#include "Swimmer.h"
#include "SwimmersCollectionLocal.h"
#include "Times.h"

TEST(SwimmersCollectionLocal, GIVEN_collection_WHEN_add_one_swimmer_THEN_size_is_1) {
  SwimmersCollectionLocal collection;
  collection.addSwimmer("Name", "Surname", 1982, Gender::MALE);
  EXPECT_EQ(collection.size(), 1);
}

TEST(SwimmersCollectionLocal, GIVEN_collection_with_1_swimmer_WHEN_add_one_swimmer_THEN_size_is_2) {
  SwimmersCollectionLocal collection;
  collection.addSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  collection.addSwimmer("Name 2", "Surname", 1982, Gender::MALE);
  EXPECT_EQ(collection.size(), 2);
}

TEST(SwimmersCollectionLocal,
     GIVEN_collection_with_1_swimmer_WHEN_remove_this_swimmer_THEN_size_is_0) {
  SwimmersCollectionLocal collection;
  collection.addSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  collection.removeSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  EXPECT_EQ(collection.size(), 0);
}

TEST(SwimmersCollectionLocal,
     GIVEN_collection_with_1_swimmer_WHEN_add_this_swimmer_again_THEN_size_is_1) {
  SwimmersCollectionLocal collection;
  collection.addSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  collection.addSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  EXPECT_EQ(collection.size(), 1);
}

TEST(SwimmersCollectionLocal, GIVEN_collection_with_2_swimmer_WHEN_clean_THEN_size_is_0) {
  SwimmersCollectionLocal collection;
  collection.addSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  collection.addSwimmer("Name 2", "Surname", 1982, Gender::MALE);
  collection.cleanAllSwimmers();
  EXPECT_EQ(collection.size(), 0);
}

TEST(SwimmersCollectionLocal,
     GIVEN_collection_with_2_swimmer_WHEN_dump_and_recover_THEN_size_of_recovered_collection_is_2) {
  SwimmersCollectionLocal collection;
  collection.addSwimmer("Name 1", "Surname", 1982, Gender::MALE);
  collection.addSwimmer("Name 2", "Surname", 1982, Gender::MALE);

  nlohmann::json collection_json     = collection;

  auto           recoveredCollection = collection_json.get<SwimmersCollectionLocal>();

  EXPECT_EQ(recoveredCollection.size(), 2);
}

TEST(
    SwimmersCollectionLocal,
    GIVEN_collection_with_1_swimmer_with_some_times_WHEN_dump_and_recover_THEN_size_of_recovered_collection_is_1_and_swimmer_has_its_times) {
  SwimmersCollectionLocal collection;
  const auto              swimmerId  = Swimmer::SwimmerId{"Name 1", "Surname", 1982, Gender::MALE};
  const Event             event1     = {Style::BUTTERFLY, 50, EventGender::MALE};
  const float             event1Time = 30;
  const Times::Pool       event1Pool = Times::_25;
  const Event             event2     = {Style::FREE, 400, EventGender::MALE};
  const float             event2Time = 60 * 4;
  const Times::Pool       event2Pool = Times::_50;

  collection.addSwimmer(swimmerId);
  auto& swimmer = collection.getSwimmer(swimmerId);

  swimmer.setEventTime(event1, event1Time, event1Pool);
  swimmer.setEventTime(event2, event2Time, event2Pool);

  nlohmann::json collection_json     = collection;

  auto           recoveredCollection = collection_json.get<SwimmersCollectionLocal>();
  const auto&    recoveredSwimmer    = recoveredCollection.getSwimmer(swimmerId);
  EXPECT_EQ(recoveredSwimmer.getTime(event1, event1Pool), event1Time);
  EXPECT_EQ(recoveredSwimmer.getTime(event2, event2Pool), event2Time);
}
