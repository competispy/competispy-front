#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>
#include <stdexcept>

#include "nlohmann/json_fwd.hpp"

#include "Event.h"
#include "RelayCategory.h"
#include "SessionLocal.h"
#include "Style.h"
#include "Swimmer.h"
#include "SwimmerLocalBuilder.h"
#include "SwimmerProxyProvider.h"
#include "TimeBuilder.h"
#include "Times.h"

class SessionLocalTest : public ::testing::Test {
public:
  SessionLocalTest() : session(swimmerProvider) {}

protected:
  SwimmerProxyProvider swimmerProvider;
  SessionLocal         session;
};

TEST_F(SessionLocalTest, GIVEN_a_session_with_1_swimmer_WHEN_check_available_THEN_is_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  EXPECT_TRUE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(SessionLocalTest,
       GIVEN_a_session_with_1_swimmer_WHEN_check_other_swimmer_available_THEN_is_not_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().name("sw1").build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder().name("sw2").build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  EXPECT_FALSE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw2")));
}

TEST_F(SessionLocalTest,
       GIVEN_a_session_with_2_swimmer_WHEN_check_available_THEN_both_are_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().name("sw1").build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder().name("sw2").build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));
  EXPECT_TRUE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
  EXPECT_TRUE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw2")));
}

TEST_F(SessionLocalTest,
       GIVEN_a_session_with_2_swimmer_WHEN_check_other_swimmer_available_THEN_is_not_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().name("sw1").build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder().name("sw2").build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder().name("sw3").build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));
  EXPECT_FALSE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw3")));
}

TEST_F(SessionLocalTest, GIVEN_a_session_with_a_swimmer_WHEN_remove_it_THEN_it_is_not_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().name("sw1").build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.removeSwimmer(swimmerProvider.getSwimmerId("sw1"));
  EXPECT_FALSE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(SessionLocalTest, GIVEN_a_session_with_a_swimmer_WHEN_use_it_THEN_it_is_not_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().name("sw1").build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  auto usedSwimmer = session.pickSwimmer(swimmerProvider.getSwimmerId("sw1"));
  EXPECT_FALSE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
  usedSwimmer = nullptr;
}

TEST_F(SessionLocalTest, GIVEN_a_session_with_an_used_swimmer_WHEN_unuse_it_THEN_it_is_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().name("sw1").build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  auto usedSwimmer = session.pickSwimmer(swimmerProvider.getSwimmerId("sw1"));
  usedSwimmer      = nullptr;
  EXPECT_TRUE(session.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_multiple_swimmers_but_one_with_time_in_a_event_WHEN_get_available_swimmers_for_event_THEN_it_is_the_ony_one) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BACK, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder()
                                 .name("sw3")
                                 .bornYear(1983)
                                 .gender(Gender::MALE)
                                 .time({Style::BREAST, 50, EventGender::MALE},
                                       TimeBuilder().seconds(30).cents(56).build())
                                 .build());
  swimmerProvider.add("sw4", SwimmerLocalBuilder()
                                 .name("sw4")
                                 .bornYear(1980)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(24).cents(63).build())
                                 .build());

  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw3"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw4"));
  const auto availableSwimmersFor50back = session.Session::getAvailableSwimmersForEvent(
      {Style::BACK, 50, EventGender::MALE}, Times::_25);
  EXPECT_EQ(availableSwimmersFor50back.size(), 1);
  EXPECT_EQ(std::get<0>(availableSwimmersFor50back[0]), swimmerProvider.getSwimmer("sw2").getId());
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_multiple_swimmers_but_2_with_time_in_a_event_WHEN_get_available_swimmers_for_event_THEN_it_both_are_and_array_is_sorted_by_time) {
  swimmerProvider.add(
      "sw1", SwimmerLocalBuilder()
                 .name("sw1")
                 .bornYear(1982)
                 .gender(Gender::MALE)
                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                       TimeBuilder().seconds(26).cents(98).build())
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(2).build())

                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BACK, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder()
                                 .name("sw3")
                                 .bornYear(1983)
                                 .gender(Gender::MALE)
                                 .time({Style::BREAST, 50, EventGender::MALE},
                                       TimeBuilder().seconds(30).cents(56).build())
                                 .build());
  swimmerProvider.add(
      "sw4", SwimmerLocalBuilder()
                 .name("sw4")
                 .bornYear(1980)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .build());

  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw3"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw4"));
  const auto availableSwimmersFor50back = session.Session::getAvailableSwimmersForEvent(
      {Style::FREE, 50, EventGender::MALE}, Times::_25);
  EXPECT_EQ(availableSwimmersFor50back.size(), 2);
  EXPECT_EQ(std::get<0>(availableSwimmersFor50back[0]), swimmerProvider.getSwimmer("sw4").getId());
  EXPECT_EQ(std::get<0>(availableSwimmersFor50back[1]), swimmerProvider.getSwimmer("sw1").getId());
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_2_swimmers_with_time_in_a_event_but_one_used_WHEN_get_available_swimmers_for_event_THEN_the_unused_is_the_only_available) {
  swimmerProvider.add(
      "sw1", SwimmerLocalBuilder()
                 .name("sw1")
                 .bornYear(1982)
                 .gender(Gender::MALE)
                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                       TimeBuilder().seconds(26).cents(98).build())
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(2).build())

                 .build());
  swimmerProvider.add(
      "sw4", SwimmerLocalBuilder()
                 .name("sw4")
                 .bornYear(1980)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .build());

  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw4"));
  auto       usedSwimmer                = session.pickSwimmer(swimmerProvider.getSwimmerId("sw1"));
  const auto availableSwimmersFor50back = session.Session::getAvailableSwimmersForEvent(
      {Style::FREE, 50, EventGender::MALE}, Times::_25);
  EXPECT_EQ(availableSwimmersFor50back.size(), 1);
  EXPECT_EQ(std::get<0>(availableSwimmersFor50back[0]), swimmerProvider.getSwimmer("sw4").getId());
  usedSwimmer = nullptr;
}

TEST_F(SessionLocalTest, GIVEN_a_session_with_1_relay_WHEN_get_it_THEN_no_throw) {
  const Event         event    = {Style::FREE, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};
  session.addRelay(relayId);
  EXPECT_NO_THROW(session.getRelay(relayId));
}

TEST_F(SessionLocalTest, GIVEN_a_session_with_1_relay_WHEN_delete_it_THEN_session_dont_has_it) {
  const Event         event    = {Style::FREE, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};
  session.addRelay(relayId);
  session.deleteRelay(relayId);
  EXPECT_FALSE(session.hasRelay(relayId));
}

TEST_F(SessionLocalTest, GIVEN_a_session_with_1_relay_WHEN_get_other_THEN_throws) {
  const Event         event              = {Style::FREE, 50, EventGender::MALE};
  const Event         not_existing_event = {Style::MEDLEY, 50, EventGender::MALE};
  const RelayCategory category           = RelayCategory::_100;
  const Times::Pool   pool               = Times::_25;
  const Relay::Id     relayId{event, category, pool};
  const Relay::Id     not_existing_relayId{not_existing_event, category, pool};
  session.addRelay(relayId);
  EXPECT_THROW(session.getRelay(not_existing_relayId), std::logic_error);
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_1_relay_WHEN_json_dump_and_recover_THEN_a_session_with_same_relay_is_recovered) {
  const Event         event    = {Style::FREE, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};
  session.addRelay(relayId);
  const auto   sessionJson = static_cast<nlohmann::json>(session);
  SessionLocal recoveredSesssion(sessionJson, swimmerProvider);
  ASSERT_NO_THROW(recoveredSesssion.getRelay(relayId));
  EXPECT_EQ(recoveredSesssion.getRelay(relayId), session.getRelay(relayId));
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_1_swimmer_WHEN_json_dump_and_recover_THEN_a_session_with_same_swimmer_is_recovered) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  const auto   sessionJson = static_cast<nlohmann::json>(session);
  SessionLocal recoveredSesssion(sessionJson, swimmerProvider);
  EXPECT_TRUE(recoveredSesssion.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_one_filled_relay_WHEN_dump_and_restore_THEN_session_has_same_filled_relay) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BACK, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder()
                                 .name("sw3")
                                 .bornYear(1983)
                                 .gender(Gender::MALE)
                                 .time({Style::BREAST, 50, EventGender::MALE},
                                       TimeBuilder().seconds(30).cents(56).build())
                                 .build());
  swimmerProvider.add("sw4", SwimmerLocalBuilder()
                                 .name("sw4")
                                 .bornYear(1980)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(24).cents(63).build())
                                 .build());

  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw3"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw4"));

  const Event         event    = {Style::MEDLEY, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};

  session.addRelay(relayId);
  auto& relay = session.getRelay(relayId);
  relay.setSwimmer(session.pickSwimmer(swimmerProvider.getSwimmerId("sw1")), 0);
  relay.setSwimmer(session.pickSwimmer(swimmerProvider.getSwimmerId("sw2")), 1);
  relay.setSwimmer(session.pickSwimmer(swimmerProvider.getSwimmerId("sw3")), 2);
  relay.setSwimmer(session.pickSwimmer(swimmerProvider.getSwimmerId("sw4")), 3);
  ASSERT_TRUE(relay.isFull());

  SessionLocal recoveredSesssion(static_cast<nlohmann::json>(session), swimmerProvider);
  auto&        recoveredRelay = session.getRelay(relayId);
  EXPECT_TRUE(recoveredRelay.isFull());
  EXPECT_EQ(recoveredRelay, relay);
  EXPECT_EQ(recoveredRelay.getAccumulatedTime(), relay.getAccumulatedTime());
  EXPECT_EQ(recoveredRelay.getSumAge(2020), relay.getSumAge(2020));
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_one_relay_two_swimmers_one_of_them_used_WHEN_dump_and_restore_THEN_one_user_used_other_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BACK, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());

  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));

  const Event         event    = {Style::MEDLEY, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};

  session.addRelay(relayId);
  auto& relay = session.getRelay(relayId);
  relay.setSwimmer(session.pickSwimmer(swimmerProvider.getSwimmerId("sw1")), 0);

  SessionLocal recoveredSesssion(static_cast<nlohmann::json>(session), swimmerProvider);
  auto&        recoveredRelay = session.getRelay(relayId);
  EXPECT_TRUE(recoveredSesssion.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw2")));
  EXPECT_TRUE(recoveredSesssion.hasSwimmer(swimmerProvider.getSwimmerId("sw1")));
  EXPECT_FALSE(recoveredSesssion.isSwimmerAvailable(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(SessionLocalTest,
       GIVEN_a_session_with_one_relay_with_1_swimmer_WHEN_remove_swimmer_THEN_relay_is_empty) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());

  // spdlog::set_level(spdlog::level::debug);  // Set global log level to debug
  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));

  const Event         event    = {Style::FREE, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};

  session.addRelay(relayId);
  auto& relay = session.getRelay(relayId);
  relay.setSwimmer(session.pickSwimmer(swimmerProvider.getSwimmerId("sw1")), 2);

  session.removeSwimmer(swimmerProvider.getSwimmerId("sw1"));
  EXPECT_EQ(relay.getLeftPositions().size(), 4);
}

TEST_F(
    SessionLocalTest,
    GIVEN_a_session_with_4_swimmers_WHEN_setting_an_available_swimmer_in_a_relay_THEN_available_swimmers_get_reduced_progresively) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder()
                                 .name("sw3")
                                 .bornYear(1983)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(30).cents(56).build())
                                 .build());
  swimmerProvider.add("sw4", SwimmerLocalBuilder()
                                 .name("sw4")
                                 .bornYear(1980)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(24).cents(63).build())
                                 .build());

  session.addSwimmer(swimmerProvider.getSwimmerId("sw1"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw2"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw3"));
  session.addSwimmer(swimmerProvider.getSwimmerId("sw4"));

  const Event         event    = {Style::FREE, 50, EventGender::MALE};
  const RelayCategory category = RelayCategory::_100;
  const Times::Pool   pool     = Times::_25;
  const Relay::Id     relayId{event, category, pool};

  session.addRelay(relayId);
  auto& relay = session.getRelay(relayId);
  for (size_t i = 0; i < 4; ++i) {
    auto swimmersForPosition
        = static_cast<const Session&>(session).getAvailableSwimmersForEvent(event, relay.getPool());
    ASSERT_EQ(swimmersForPosition.size(), 4 - i);
    relay.setSwimmer(session.pickSwimmer(std::get<0>(swimmersForPosition.front())), i);
  }
  ASSERT_TRUE(relay.isFull());
}
