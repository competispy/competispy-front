add_executable(local_models_tests
  RelayLocalTest.cpp
  SessionLocalTest.cpp
  SwimmerLocalTest.cpp
  SwimmersCollectionLocalTest.cpp
  TeamLocalTest.cpp
)

target_link_libraries(local_models_tests GTest::gmock_main local_models test_utils)

include(GoogleTest)
gtest_discover_tests(local_models_tests)

