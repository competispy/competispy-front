#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>
#include <unordered_map>

#include "nlohmann/json_fwd.hpp"

#include "Event.h"
#include "Gender.h"
#include "RelayCategory.h"
#include "RelayLocal.h"
#include "Style.h"
#include "Swimmer.h"
#include "SwimmerLocal.h"
#include "SwimmerLocalBuilder.h"
#include "SwimmerProxy.h"
#include "SwimmerProxyProvider.h"
#include "SwimmersCollection.h"
#include "TimeBuilder.h"
#include "Times.h"

/*
  setSwimmer
  unsetSwimmer
  removeSwimmer
  isFull
  getSumAge
  getPositionData
  getLeftPositions
  getNewSwimmerAgeRange
  getGenderCount
  needsSwimmerFromGender
  getAccumulatedTime
  Equal operator (event and category)
*/

TEST(RelayLocal, GIVEN_a_full_relay_WHEN_check_full_THEN_is_true) {
  auto sw1 = SwimmerLocalBuilder()
                 .name("sw1")
                 .bornYear(1982)
                 .gender(Gender::MALE)
                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                       TimeBuilder().seconds(26).cents(98).build())
                 .buildPtr();
  auto sw2
      = SwimmerLocalBuilder()
            .name("sw2")
            .bornYear(1982)
            .gender(Gender::MALE)
            .time({Style::BACK, 50, EventGender::MALE}, TimeBuilder().seconds(28).cents(87).build())
            .buildPtr();
  auto sw3 = SwimmerLocalBuilder()
                 .name("sw3")
                 .bornYear(1983)
                 .gender(Gender::MALE)
                 .time({Style::BREAST, 50, EventGender::MALE},
                       TimeBuilder().seconds(30).cents(56).build())
                 .buildPtr();
  auto sw4
      = SwimmerLocalBuilder()
            .name("sw4")
            .bornYear(1980)
            .gender(Gender::MALE)
            .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(24).cents(63).build())
            .buildPtr();
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(std::move(sw1), 0);
  relay.Relay::setSwimmer(std::move(sw2), 1);
  relay.Relay::setSwimmer(std::move(sw3), 2);
  relay.Relay::setSwimmer(std::move(sw4), 3);
  EXPECT_TRUE(relay.isFull());
}

TEST(RelayLocal, GIVEN_an_empty_relay_WHEN_get_time_THEN_is_0) {
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  EXPECT_EQ(relay.getAccumulatedTime(), 0);
}

TEST(RelayLocal, GIVEN_a_male_medley_relay_with_1_swimmer_with_time_1_WHEN_get_time_THEN_is_1) {
  auto breastSwimmer
      = SwimmerLocalBuilder()
            .name("sw3")
            .bornYear(1983)
            .gender(Gender::MALE)
            .time({Style::BREAST, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
            .buildPtr();
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(std::move(breastSwimmer), 1);
  EXPECT_EQ(relay.getAccumulatedTime(), 1);
}

TEST(RelayLocal, GIVEN_a_male_free_relay_with_1_swimmer_with_time_1_WHEN_get_time_THEN_is_1) {
  auto sw3 = SwimmerLocalBuilder()
                 .name("sw3")
                 .bornYear(1983)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .buildPtr();
  RelayLocal relay({Style::FREE, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(std::move(sw3), 2);
  EXPECT_EQ(relay.getAccumulatedTime(), 1);
}

TEST(RelayLocal, GIVEN_a_full_male_medley_relay_with_swimmer_with_time_1_WHEN_get_time_THEN_is_4) {
  auto butterflySwimmer
      = SwimmerLocalBuilder()
            .name("sw1")
            .bornYear(1982)
            .gender(Gender::MALE)
            .time({Style::BUTTERFLY, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
            .buildPtr();
  auto backSwimmer
      = SwimmerLocalBuilder()
            .name("sw2")
            .bornYear(1982)
            .gender(Gender::MALE)
            .time({Style::BACK, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
            .buildPtr();
  auto breastSwimmer
      = SwimmerLocalBuilder()
            .name("sw3")
            .bornYear(1983)
            .gender(Gender::MALE)
            .time({Style::BREAST, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
            .buildPtr();
  auto freeSwimmer
      = SwimmerLocalBuilder()
            .name("sw4")
            .bornYear(1980)
            .gender(Gender::MALE)
            .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
            .buildPtr();
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(std::move(backSwimmer), 0);
  relay.Relay::setSwimmer(std::move(breastSwimmer), 1);
  relay.Relay::setSwimmer(std::move(butterflySwimmer), 2);
  relay.Relay::setSwimmer(std::move(freeSwimmer), 3);
  EXPECT_EQ(relay.getAccumulatedTime(), 4);
}

TEST(RelayLocal, GIVEN_a_full_male_free_relay_with_swimmer_with_time_1_WHEN_get_time_THEN_is_4) {
  auto sw1 = SwimmerLocalBuilder()
                 .name("sw1")
                 .bornYear(1982)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .buildPtr();
  auto sw2 = SwimmerLocalBuilder()
                 .name("sw2")
                 .bornYear(1982)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .buildPtr();
  auto sw3 = SwimmerLocalBuilder()
                 .name("sw3")
                 .bornYear(1983)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .buildPtr();
  auto sw4 = SwimmerLocalBuilder()
                 .name("sw4")
                 .bornYear(1980)
                 .gender(Gender::MALE)
                 .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
                 .buildPtr();
  RelayLocal relay({Style::FREE, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(std::move(sw1), 0);
  relay.Relay::setSwimmer(std::move(sw2), 1);
  relay.Relay::setSwimmer(std::move(sw3), 2);
  relay.Relay::setSwimmer(std::move(sw4), 3);
  EXPECT_EQ(relay.getAccumulatedTime(), 4);
}

TEST(RelayLocal, GIVEN_an_empty_relay_WHEN_getLeftPositions_THEN_4_is_obtained) {
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  EXPECT_EQ(relay.getLeftPositions().size(), 4);
}

TEST(RelayLocal,
     GIVEN_a_relay_with_1_proxy_swimmer_WHEN_proxy_swimmer_is_deleted_THEN_relay_is_empty) {
  auto swimmer
      = SwimmerLocalBuilder()
            .name("sw1")
            .bornYear(1982)
            .gender(Gender::MALE)
            .time({Style::BUTTERFLY, 50, EventGender::MALE}, TimeBuilder().seconds(1).build())
            .buildPtr();
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  auto       swimmerProxy = std::make_unique<SwimmerProxy>(*swimmer);
  auto       relaySwimmer = std::make_unique<SwimmerProxy>(static_cast<Swimmer&>(*swimmerProxy));

  relay.Relay::setSwimmer(std::move(relaySwimmer), 2);
  swimmerProxy = nullptr;
  EXPECT_EQ(relay.getLeftPositions().size(), 4);
}

TEST(
    RelayLocal,
    GIVEN_a_relay_filled_with_swimmers_WHEN_a_new_relay_is_created_from_its_json_representation_THEN_the_new_relay_json_representation_is_the_same_as_the_original_one) {
  SwimmerProxyProvider swimmerProvider;
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BACK, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder()
                                 .name("sw3")
                                 .bornYear(1983)
                                 .gender(Gender::MALE)
                                 .time({Style::BREAST, 50, EventGender::MALE},
                                       TimeBuilder().seconds(30).cents(56).build())
                                 .build());
  swimmerProvider.add("sw4", SwimmerLocalBuilder()
                                 .name("sw4")
                                 .bornYear(1980)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(24).cents(63).build())
                                 .build());
  RelayLocal relay({Style::FREE, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw1"), 0);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw2"), 1);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw3"), 2);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw4"), 3);

  const nlohmann::json relayJson = static_cast<nlohmann::json>(relay);
  RelayLocal           recoveredRelay(relayJson, [&swimmerProvider](const Swimmer::SwimmerId id) {
    return swimmerProvider.getSwimmerPtr(id);
  });
  EXPECT_EQ(static_cast<nlohmann::json>(recoveredRelay), relayJson);
}

TEST(
    RelayLocal,
    GIVEN_a_relay_with_2_swimmers_WHEN_a_new_relay_is_created_from_its_json_representation_THEN_the_new_relay_json_representation_is_the_same_as_the_original_one) {
  SwimmerProxyProvider swimmerProvider;
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .bornYear(1982)
                                 .gender(Gender::MALE)
                                 .time({Style::BUTTERFLY, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw4", SwimmerLocalBuilder()
                                 .name("sw4")
                                 .bornYear(1980)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(24).cents(63).build())
                                 .build());
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw1"), 0);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw4"), 3);

  const nlohmann::json relayJson = static_cast<nlohmann::json>(relay);
  RelayLocal           recoveredRelay(relayJson, [&swimmerProvider](const Swimmer::SwimmerId id) {
    return swimmerProvider.getSwimmerPtr(id);
  });
  EXPECT_EQ(static_cast<nlohmann::json>(recoveredRelay), relayJson);
}

TEST(
    RelayLocal,
    GIVEN_an_empty_relay_WHEN_a_new_relay_is_created_from_its_json_representation_THEN_the_new_relay_json_representation_is_the_same_as_the_original_one_and_relays_are_equal) {
  SwimmerProxyProvider swimmerProvider;
  RelayLocal relay({Style::MEDLEY, 50, EventGender::MALE}, RelayCategory::_120, Times::_25);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw1"), 0);
  relay.Relay::setSwimmer(swimmerProvider.getSwimmerPtr("sw4"), 3);

  const nlohmann::json relayJson = static_cast<nlohmann::json>(relay);
  RelayLocal           recoveredRelay(relayJson, [&swimmerProvider](const Swimmer::SwimmerId id) {
    return swimmerProvider.getSwimmerPtr(id);
  });
  EXPECT_EQ(static_cast<nlohmann::json>(recoveredRelay), relayJson);
  EXPECT_EQ(recoveredRelay, relay);
}
