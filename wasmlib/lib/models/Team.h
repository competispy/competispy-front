#ifndef COMPETYSPY_FRONT_TEAM_H
#define COMPETYSPY_FRONT_TEAM_H

#include <cstddef>
#include <string>

#include "Session.h"
#include "Swimmer.h"
#include "SwimmerProvider.h"

class Team : public SwimmerProvider {
public:
  ~Team() override                                                                = default;
  virtual void               addSession(const std::string& sessionId)             = 0;
  virtual void               removeSession(const std::string& sessionId)          = 0;
  virtual auto               getSession(const std::string& sessionId) -> Session& = 0;
  [[nodiscard]] virtual auto getSession(const std::string& sessionId) const -> const Session& = 0;
  [[nodiscard]] virtual auto hasSession(const std::string& sessionId) const -> bool           = 0;
  [[nodiscard]] virtual auto getSessionIds() const -> std::vector<std::string>                = 0;

  [[nodiscard]] virtual auto getSwimmersSize() const -> size_t                                = 0;

  void addSwimmerToSession(const Swimmer::SwimmerId& swimmerId, const std::string& sessionId) {
    getSession(sessionId).addSwimmer(swimmerId);
  }

  void addSwimmerToSession(const Swimmer& swimmer, const std::string& sessionId) {
    addSwimmerToSession(swimmer.getId(), sessionId);
  }

  virtual void addSwimmer(const Swimmer::SwimmerId& swimmer) = 0;

  void         addSwimmer(const Swimmer& swimmer) { addSwimmer(swimmer.getId()); }

  virtual void removeSwimmer(const Swimmer::SwimmerId& swimmer) = 0;

  void         removeSwimmer(const Swimmer& swimmer) { removeSwimmer(swimmer.getId()); }

  [[nodiscard]] virtual auto hasSwimmer(const Swimmer::SwimmerId& swimmerId) const -> bool = 0;

  [[nodiscard]] auto         hasSwimmer(const Swimmer& swimmer) const -> bool {
    return hasSwimmer(swimmer.getId());
  }

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override = 0;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override             = 0;
  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override
      = 0;
};

#endif
