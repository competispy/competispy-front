#ifndef COMPETYSPY_FRONT_TEAMLOADCONTROLLER_H
#define COMPETYSPY_FRONT_TEAMLOADCONTROLLER_H

#include "Swimmer.h"
#include "SwimmerOperationsController.h"
#include "TeamOperationsController.h"

class TeamLoadController {
public:
  TeamLoadController(TeamOperationsController&    teamOperationsController,
                     SwimmerOperationsController& swimmerOperationsController,
                     const std::string&           teamName) :
      teamModificationsController_(
          teamOperationsController.getOrCreateTeamModificationController(teamName, defaultYear)),
      swimmerOperationsController_(swimmerOperationsController) {
    if (not teamModificationsController_.hasSession(std::string(defaultSession))) {
      teamModificationsController_.addSession(std::string(defaultSession));
    }
  }

  auto getSwimmerTimesControllerForNewSwimmer(const Swimmer::SwimmerId& swimmerId)
      -> SwimmerTimesController {
    if (!swimmerOperationsController_.hasSwimmer(swimmerId)) {
      swimmerOperationsController_.addSwimmer(swimmerId);
      teamModificationsController_.addSwimmer(swimmerId);
      teamModificationsController_.getSessionOperationController(std::string(defaultSession))
          .addSwimmer(swimmerId);
    }
    return swimmerOperationsController_.getSwimmerTimesController(swimmerId);
  }

  auto getSwimmerTimesControllerForNewSwimmer(std::string name, std::string surname,
                                              uint16_t bornYear,
                                              Gender   gender) -> SwimmerTimesController {
    const Swimmer::SwimmerId swimmerId(std::move(name), std::move(surname), bornYear, gender);
    return getSwimmerTimesControllerForNewSwimmer(swimmerId);
  }

protected:
private:
  TeamModificationsController       teamModificationsController_;
  SwimmerOperationsController&      swimmerOperationsController_;
  static constexpr std::string_view defaultSession = "1";
  static constexpr uint16_t         defaultYear    = 2000;
};

#endif  // COMPETYSPY_FRONT_TEAMLOADCONTROLLER_H
