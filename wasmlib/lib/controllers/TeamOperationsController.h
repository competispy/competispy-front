#ifndef COMPETYSPY_FRONT_TEAM_OPERATIONS_CONTROLLER_H
#define COMPETYSPY_FRONT_TEAM_OPERATIONS_CONTROLLER_H

#include <string>

#include "TeamModificationsController.h"

class TeamOperationsController {
public:
  virtual void               addTeam(const std::string& teamName)                      = 0;
  [[nodiscard]] virtual auto getTeam(const std::string& teamName) const -> const Team& = 0;
  [[nodiscard]] virtual auto hasTeam(const std::string& teamName) const -> bool        = 0;

  virtual auto               getOrCreateTeam(const std::string& teamName) -> const Team& {
    return getOrCreateModificableTeam(teamName);
  }

  auto getTeamModificationController(const std::string& teamName, uint16_t year)
      -> TeamModificationsController {
    spdlog::debug("getTeamModificationController");
    return {getModificableTeam(teamName), year};
  }

  auto getOrCreateTeamModificationController(const std::string& teamName, uint16_t year)
      -> TeamModificationsController {
    return {getOrCreateModificableTeam(teamName), year};
  }

  [[nodiscard]] auto getSwimmerIds(const std::string& teamName) const
      -> std::vector<Swimmer::SwimmerId> {
    std::vector<Swimmer::SwimmerId> swimmers;
    auto                            it = getTeam(teamName).getSwimmerIterator();
    while (it->hasMore()) { swimmers.emplace_back(it->getNext().getId()); }
    return swimmers;
  }

  [[nodiscard]] auto getSessionIds(const std::string& teamName) const -> std::vector<std::string> {
    return getTeam(teamName).getSessionIds();
  }

protected:
  [[nodiscard]] virtual auto getModificableTeam(const std::string& teamName) -> Team& = 0;

  auto                       getOrCreateModificableTeam(const std::string& teamName) -> Team& {
    if (not hasTeam(teamName)) { addTeam(teamName); }
    return getModificableTeam(teamName);
  }
};

#endif
