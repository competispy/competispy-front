#include "TeamOperationsControllerLocal.h"

#include "nlohmann/json.hpp"

using nlohmann::json;

TeamOperationsControllerLocal::TeamOperationsControllerLocal(SwimmerProvider& swimmerProvider) :
    swimmerProvider_(swimmerProvider) {}

TeamOperationsControllerLocal::TeamOperationsControllerLocal(json&            jsonValue,
                                                             SwimmerProvider& swimmerProvider) :
    TeamOperationsControllerLocal(swimmerProvider) {
  for (const auto& [teamName, teamJson] : jsonValue.items()) {
    teams_.try_emplace(teamName, teamJson, swimmerProvider);
  }
}

void TeamOperationsControllerLocal::addTeam(const std::string& teamName) {
  teams_.try_emplace(teamName, swimmerProvider_);
}

auto TeamOperationsControllerLocal::getTeam(const std::string& teamName) const -> const Team& {
  return teams_.at(teamName);
}

auto TeamOperationsControllerLocal::getModificableTeam(const std::string& teamName) -> Team& {
  spdlog::debug("getModificableTeam {}", teamName);
  return teams_.at(teamName);
}

auto TeamOperationsControllerLocal::hasTeam(const std::string& teamName) const -> bool {
  return teams_.contains(teamName);
}

TeamOperationsControllerLocal::operator json() const { return static_cast<nlohmann::json>(teams_); }
