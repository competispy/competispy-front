#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <stdexcept>

#include "SwimmerLocalBuilder.h"
#include "SwimmerProxyProvider.h"
#include "TeamOperationsControllerLocal.h"

class TeamOperationsControllerLocalTest : public ::testing::Test {
public:
  TeamOperationsControllerLocalTest() : controller(swimmerProvider) {}

protected:
  SwimmerProxyProvider          swimmerProvider;
  TeamOperationsControllerLocal controller;
};

TEST_F(TeamOperationsControllerLocalTest,
       GIVEN_team_name_WHEN_add_team_and_a_swimmer_THEN_swimmer_can_be_obtained) {
  const std::string teamName = "team";
  const uint16_t    year     = 2000;
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  controller.addTeam(teamName);
  auto modificationController = controller.getTeamModificationController(teamName, year);
  modificationController.addSwimmer(swimmerProvider.getSwimmer("sw1"));
  const auto& constController = controller;
  const auto& team            = constController.getTeam(teamName);
  const auto& swimmer         = team.getSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  EXPECT_EQ(swimmer.getId(), swimmerProvider.getSwimmer("sw1").getId());
}

TEST_F(TeamOperationsControllerLocalTest, GIVEN_team_WHEN_add_a_session_THEN_) {
  //
}
