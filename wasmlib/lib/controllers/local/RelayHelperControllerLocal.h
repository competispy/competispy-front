#ifndef COMPETYSPY_FRONT_RELAY_HELPER_CONTROLLER_LOCAL_H
#define COMPETYSPY_FRONT_RELAY_HELPER_CONTROLLER_LOCAL_H

#include <memory>

#include "nlohmann/json_fwd.hpp"

#include "RelayHelperController.h"
#include "SwimmerOperationsControllerLocal.h"
#include "TeamOperationsControllerLocal.h"

class RelayHelperControllerLocal : public RelayHelperController {
public:
  RelayHelperControllerLocal() :
      swimmerOperationsController_(std::make_unique<SwimmerOperationsControllerLocal>()),
      teamOperationsController_(
          std::make_unique<TeamOperationsControllerLocal>(*swimmerOperationsController_)) {
    spdlog::debug("[RelayHelperControllerLocal] constructor");
  }

  RelayHelperControllerLocal(nlohmann::json& jsonValue) :
      swimmerOperationsController_(
          std::make_unique<SwimmerOperationsControllerLocal>(jsonValue["swimmers"])),
      teamOperationsController_(std::make_unique<TeamOperationsControllerLocal>(
          jsonValue["teams"], *swimmerOperationsController_)) {}

  ~RelayHelperControllerLocal() override = default;

  auto getSwimmerOperationsController() -> SwimmerOperationsController& override {
    return *swimmerOperationsController_;
  }

  auto getTeamOperationsController() -> TeamOperationsController& override {
    return *teamOperationsController_;
  }

  operator nlohmann::json() const {
    nlohmann::json jsonValue;
    jsonValue["swimmers"] = static_cast<nlohmann::json>(*swimmerOperationsController_);
    jsonValue["teams"]    = static_cast<nlohmann::json>(*teamOperationsController_);
    return jsonValue;
  }

  void loadBackup(nlohmann::json jsonValue) {
    swimmerOperationsController_
        = std::make_unique<SwimmerOperationsControllerLocal>(jsonValue["swimmers"]);
    teamOperationsController_ = std::make_unique<TeamOperationsControllerLocal>(
        jsonValue["teams"], *swimmerOperationsController_);
  }

protected:
private:
  std::unique_ptr<SwimmerOperationsControllerLocal> swimmerOperationsController_;
  std::unique_ptr<TeamOperationsControllerLocal>    teamOperationsController_;
};

#endif
