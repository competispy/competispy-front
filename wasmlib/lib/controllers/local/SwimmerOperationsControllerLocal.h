#ifndef COMPETYSPY_FRONT_SWIMMER_OPERATIONS_CONTROLLER_LOCAL_H
#define COMPETYSPY_FRONT_SWIMMER_OPERATIONS_CONTROLLER_LOCAL_H

#include <SwimmerOperationsController.h>
#include <SwimmerProvider.h>

#include "SwimmersCollectionLocal.h"

class SwimmerOperationsControllerLocal : public SwimmerOperationsController {
public:
  SwimmerOperationsControllerLocal() = default;
  SwimmerOperationsControllerLocal(nlohmann::json& jsonValue);
  ~SwimmerOperationsControllerLocal() override = default;

  void               addSwimmer(const Swimmer::SwimmerId& id) override;
  void               removeSwimmer(const Swimmer::SwimmerId& id) override;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override;
  [[nodiscard]] auto hasSwimmer(const Swimmer::SwimmerId& id) const -> bool override;

  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override;

  operator nlohmann::json() const;

protected:
  auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override;

private:
  SwimmersCollectionLocal swimmers_;
};

#endif
