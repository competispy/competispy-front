#ifndef COMPETYSPY_FRONT_SESSIONOPERATIONSCONTROLLER_H
#define COMPETYSPY_FRONT_SESSIONOPERATIONSCONTROLLER_H

#include "Relay.h"
#include "RelayOperationsController.h"
#include "RelaySummaryController.h"
#include "Session.h"

class SessionOperationsController {
public:
  explicit SessionOperationsController(Session& session, uint16_t year) :
      session_(session),
      year_(year) {}

  void addSwimmer(const Swimmer& swimmer) { session_.addSwimmer(swimmer.getId()); }

  void addSwimmer(const Swimmer::SwimmerId& swimmer) { session_.addSwimmer(swimmer); }

  void removeSwimmer(const Swimmer& swimmer) { session_.removeSwimmer(swimmer.getId()); }

  void removeSwimmer(const Swimmer::SwimmerId& swimmer) { session_.removeSwimmer(swimmer); }

  [[nodiscard]] auto hasSwimmer(const Swimmer& swimmer) const -> bool {
    return session_.hasSwimmer(swimmer.getId());
  }

  [[nodiscard]] auto hasSwimmer(const Swimmer::SwimmerId& swimmer) const -> bool {
    return session_.hasSwimmer(swimmer);
  }

  [[nodiscard]] auto isSwimmerAvailable(const Swimmer& swimmer) const -> bool {
    return session_.isSwimmerAvailable(swimmer.getId());
  }

  [[nodiscard]] auto isSwimmerAvailable(const Swimmer::SwimmerId& swimmer) const -> bool {
    return session_.isSwimmerAvailable(swimmer);
  }

  void addRelay(const Relay::Id& relayId) { session_.addRelay(relayId); }

  auto getRelay(const Relay::Id& relayId) -> const Relay& { return session_.getRelay(relayId); }

  void deleteRelay(const Relay::Id& relayId) { session_.deleteRelay(relayId); }

  [[nodiscard]] auto hasRelay(const Relay::Id& relayId) const -> bool {
    return session_.hasRelay(relayId);
  }

  void addRelay(Event event, RelayCategory category, Times::Pool pool) {
    addRelay({event, category, pool});
  }

  auto getRelay(Event event, RelayCategory category, Times::Pool pool) -> const Relay& {
    return getRelay({event, category, pool});
  }

  void deleteRelay(Event event, RelayCategory category, Times::Pool pool) {
    deleteRelay({event, category, pool});
  }

  [[nodiscard]] auto hasRelay(Event event, RelayCategory category, Times::Pool pool) const -> bool {
    return hasRelay({event, category, pool});
  }

  [[nodiscard]] auto getAvailableSwimmersForEvent(const Event& event, Times::Pool pool) const
      -> SwimmerEventCollection::Collection {
    return session_.getAvailableSwimmersForEvent(event, pool);
  }

  auto getRelayOperationsController(Relay::Id relayId) -> RelayOperationsController {
    return RelayOperationsController{session_.getRelay(relayId), session_, year_};
  }

  auto getRelayOperationsController(Event event, RelayCategory category, Times::Pool pool)
      -> RelayOperationsController {
    return getRelayOperationsController({event, category, pool});
  }

  auto getRelaySummaryController(const Relay::Id& relayId) -> RelaySummaryController {
    return {getRelay(relayId)};
  }

private:
  Session& session_;
  uint16_t year_;
};

#endif  // COMPETYSPY_FRONT_SESSIONOPERATIONSCONTROLLER_H
