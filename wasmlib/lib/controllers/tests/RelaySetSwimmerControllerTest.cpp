#include <gtest/gtest.h>

#include <cstdint>
#include <string>
#include <tuple>

#include "Event.h"
#include "Gender.h"
#include "RelayCategory.h"
#include "RelayHelperControllerLocal.h"
#include "Style.h"
#include "Swimmer.h"

class RelaySetSwimmerControllerTest : public ::testing::Test {
public:
  using EasySwimmer = std::tuple<std::string, uint16_t>;

  RelaySetSwimmerControllerTest() {
    auto controller
        = relayHelperController.getTeamOperationsController().getOrCreateTeamModificationController(
            getTeamName(), defaultYear);
    controller.addSession(getSessionId());
    controller.getSessionOperationController(getSessionId()).addRelay(defaultRelayId);
  }

  auto getController(uint8_t position) {
    relayHelperController.getTeamOperationsController().addTeam(getTeamName());

    return relayHelperController.getRelaySetSwimmerController(
        getTeamName(), getSessionId(), defaultRelayId, defaultYear, position);
  }

  static auto getTeamName() -> std::string { return std::string{teamName}; }

  static auto getSessionId() -> std::string { return std::string{sessionId}; }

  static auto getSwimmerId(const std::string& name, uint16_t bornYear) -> Swimmer::SwimmerId {
    return {name, "", bornYear, Gender::MALE};
  }

  void addSwimmer(const EasySwimmer& swimmer) {
    std::apply([this](const std::string& name, uint16_t year) { addSwimmer(name, year); }, swimmer);
  }

  void addSwimmer(const std::string& name, uint16_t bornYear) {
    auto controller = relayHelperController.getTeamLoadController(getTeamName())
                          .getSwimmerTimesControllerForNewSwimmer(name, "", bornYear, Gender::MALE);
    controller.setEventTime(defaultEvent, 30, defaultPool);
  }

  void setSwimmer(const EasySwimmer& swimmer, uint8_t position) {
    getController(position).setSwimmer(std::apply(
        [this](const std::string& name, uint16_t year) { return getSwimmerId(name, year); },
        swimmer));
  }

  auto getAvailableSwimmers(uint8_t position) {
    return getController(position).getAvailableSwimmers();
  }

  static auto           position(uint8_t value) -> uint8_t { return value; }

  static constexpr auto bornYearForAge(uint16_t value) -> uint16_t { return defaultYear - value; }

protected:
  RelayHelperControllerLocal        relayHelperController;
  static constexpr std::string_view teamName        = "team";
  static constexpr std::string_view sessionId       = "1";
  static constexpr uint8_t          defaultDistance = 50;
  static constexpr Event            defaultEvent{Style::FREE, defaultDistance, EventGender::MALE};
  static constexpr RelayCategory    defaultCategory = RelayCategory::_120;
  static constexpr Times::Pool      defaultPool     = Times::Pool::_25;
  static constexpr Relay::Id        defaultRelayId{defaultEvent, defaultCategory, defaultPool};
  static constexpr uint64_t         defaultYear = 2020;
};

TEST_F(
    RelaySetSwimmerControllerTest,
    GIVEN_a_session_with_a_relay_and_4_swimmers_WHEN_get_available_swimmers_THEN_4_are_available) {
  const EasySwimmer sw1{"sw1", bornYearForAge(30)};
  const EasySwimmer sw2{"sw2", bornYearForAge(30)};
  const EasySwimmer sw3{"sw3", bornYearForAge(30)};
  const EasySwimmer sw4{"sw4", bornYearForAge(30)};

  addSwimmer(sw1);
  addSwimmer(sw2);
  addSwimmer(sw3);
  addSwimmer(sw4);

  EXPECT_EQ(getAvailableSwimmers(position(0)).size(), 4);
}

TEST_F(
    RelaySetSwimmerControllerTest,
    GIVEN_a_session_with_a_relay_and_4_swimmers_but_one_already_selected_WHEN_get_available_swimmers_for_used_position_THEN_4_are_available) {
  const EasySwimmer sw1{"sw1", bornYearForAge(30)};
  const EasySwimmer sw2{"sw2", bornYearForAge(30)};
  const EasySwimmer sw3{"sw3", bornYearForAge(30)};
  const EasySwimmer sw4{"sw4", bornYearForAge(30)};

  addSwimmer(sw1);
  addSwimmer(sw2);
  addSwimmer(sw3);
  addSwimmer(sw4);

  setSwimmer(sw1, position(0));
  EXPECT_EQ(getAvailableSwimmers(position(0)).size(), 4);
}

TEST_F(
    RelaySetSwimmerControllerTest,
    GIVEN_a_session_with_a_relay_and_4_swimmers_but_one_already_selected_WHEN_get_available_swimmers_for_unused_position_THEN_3_are_available) {
  const EasySwimmer sw1{"sw1", bornYearForAge(30)};
  const EasySwimmer sw2{"sw2", bornYearForAge(30)};
  const EasySwimmer sw3{"sw3", bornYearForAge(30)};
  const EasySwimmer sw4{"sw4", bornYearForAge(30)};

  addSwimmer(sw1);
  addSwimmer(sw2);
  addSwimmer(sw3);
  addSwimmer(sw4);

  setSwimmer(sw1, position(0));
  EXPECT_EQ(getAvailableSwimmers(position(1)).size(), 3);
}

TEST_F(
    RelaySetSwimmerControllerTest,
    GIVEN_a_session_with_a_relay_and_4_swimmers_but_relay_is_filled_WHEN_get_available_swimmers_for_any_position_THEN_1_is_available) {
  const EasySwimmer sw1{"sw1", bornYearForAge(30)};
  const EasySwimmer sw2{"sw2", bornYearForAge(30)};
  const EasySwimmer sw3{"sw3", bornYearForAge(30)};
  const EasySwimmer sw4{"sw4", bornYearForAge(30)};

  addSwimmer(sw1);
  addSwimmer(sw2);
  addSwimmer(sw3);
  addSwimmer(sw4);

  setSwimmer(sw1, position(0));
  setSwimmer(sw2, position(1));
  setSwimmer(sw3, position(2));
  setSwimmer(sw4, position(3));

  EXPECT_EQ(getAvailableSwimmers(position(0)).size(), 1);
  EXPECT_EQ(getAvailableSwimmers(position(1)).size(), 1);
  EXPECT_EQ(getAvailableSwimmers(position(2)).size(), 1);
  EXPECT_EQ(getAvailableSwimmers(position(3)).size(), 1);
}
