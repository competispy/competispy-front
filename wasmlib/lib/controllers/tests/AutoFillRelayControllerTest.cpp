#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <stdexcept>

#include "AutoFillRelayController.h"
#include "SessionLocal.h"
#include "SwimmerLocalBuilder.h"
#include "SwimmerProxyProvider.h"
#include "TimeBuilder.h"

class AutoFillRelayControllerTest : public ::testing::Test {
public:
  AutoFillRelayControllerTest() : session(swimmerProvider) {}

protected:
  SwimmerProxyProvider swimmerProvider;
  SessionLocal         session;
};

TEST_F(
    AutoFillRelayControllerTest,
    GIVEN_a_session_with_4_swimmers_and_empty_medley_relay_WHEN_fill_THEN_is_filled_with_swimmers_in_their_positions) {
  const uint16_t year = 100;
  swimmerProvider.add("butterfly", SwimmerLocalBuilder()
                                       .name("butterfly")
                                       .age(year, 30)
                                       .gender(Gender::MALE)
                                       .time({Style::BUTTERFLY, 50, EventGender::MALE},
                                             TimeBuilder().seconds(26).cents(98).build())
                                       .build());
  swimmerProvider.add("back", SwimmerLocalBuilder()
                                  .name("back")
                                  .age(year, 30)
                                  .gender(Gender::MALE)
                                  .time({Style::BACK, 50, EventGender::MALE},
                                        TimeBuilder().seconds(28).cents(87).build())
                                  .build());
  swimmerProvider.add("breast", SwimmerLocalBuilder()
                                    .name("breast")
                                    .age(year, 29)
                                    .gender(Gender::MALE)
                                    .time({Style::BREAST, 50, EventGender::MALE},
                                          TimeBuilder().seconds(30).cents(56).build())
                                    .build());
  swimmerProvider.add("free", SwimmerLocalBuilder()
                                  .name("free")
                                  .age(year, 32)
                                  .gender(Gender::MALE)
                                  .time({Style::FREE, 50, EventGender::MALE},
                                        TimeBuilder().seconds(24).cents(63).build())
                                  .build());

  session.addSwimmer(swimmerProvider.getSwimmer("butterfly").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("back").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("breast").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("free").getId());
  const Relay::Id relayId{
    {Style::MEDLEY, 50, EventGender::MALE},
    RelayCategory::_120, Times::Pool::_25
  };
  session.addRelay(relayId);
  Relay&                  relay = session.getRelay(relayId);
  AutoFillRelayController controller{relay, session, year};
  controller.fill();
  EXPECT_TRUE(relay.isFull());
  const auto relaySwimmers = relay.getSwimmerIds();
  EXPECT_EQ(*relaySwimmers[0], swimmerProvider.getSwimmer("back").getId());
  EXPECT_EQ(*relaySwimmers[1], swimmerProvider.getSwimmer("breast").getId());
  EXPECT_EQ(*relaySwimmers[2], swimmerProvider.getSwimmer("butterfly").getId());
  EXPECT_EQ(*relaySwimmers[3], swimmerProvider.getSwimmer("free").getId());
}

TEST_F(
    AutoFillRelayControllerTest,
    GIVEN_a_session_with_6_swimmers_and_empty_free_relay_WHEN_fill_THEN_is_filled_with_swimmers_in_best_order) {
  // spdlog::set_level(spdlog::level::debug);
  const uint16_t year = 100;
  swimmerProvider.add("sw1", SwimmerLocalBuilder()
                                 .name("sw1")
                                 .age(year, 30)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(26).cents(98).build())
                                 .build());
  swimmerProvider.add("sw2", SwimmerLocalBuilder()
                                 .name("sw2")
                                 .age(year, 30)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(28).cents(87).build())
                                 .build());
  swimmerProvider.add("sw3", SwimmerLocalBuilder()
                                 .name("sw3")
                                 .age(year, 30)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(30).cents(56).build())
                                 .build());
  swimmerProvider.add(
      "a", SwimmerLocalBuilder()
               .name("a")
               .age(year, 30)
               .gender(Gender::MALE)
               .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(31).build())
               .build());
  swimmerProvider.add(
      "b", SwimmerLocalBuilder()
               .name("b")
               .age(year, 30)
               .gender(Gender::MALE)
               .time({Style::FREE, 50, EventGender::MALE}, TimeBuilder().seconds(32).build())
               .build());
  swimmerProvider.add("sw4", SwimmerLocalBuilder()
                                 .name("sw4")
                                 .bornYear(year - 30)
                                 .gender(Gender::MALE)
                                 .time({Style::FREE, 50, EventGender::MALE},
                                       TimeBuilder().seconds(24).cents(63).build())
                                 .build());

  spdlog::debug("SwimmerProvider created");

  session.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("sw2").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("sw3").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("sw4").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("a").getId());
  session.addSwimmer(swimmerProvider.getSwimmer("b").getId());
  spdlog::debug("Swimmers added to session");
  const Relay::Id relayId{
    {Style::FREE, 50, EventGender::MALE},
    RelayCategory::_120, Times::Pool::_25
  };
  session.addRelay(relayId);
  spdlog::debug("Relay added to session");
  Relay&                  relay = session.getRelay(relayId);
  AutoFillRelayController controller{relay, session, year};
  spdlog::debug("Starting to fill relay");
  controller.fill();
  spdlog::debug("Relay auto filled");
  EXPECT_TRUE(relay.isFull());
  const auto relaySwimmers = relay.getSwimmerIds();
  EXPECT_EQ(*relaySwimmers[0], swimmerProvider.getSwimmer("sw4").getId());
  EXPECT_EQ(*relaySwimmers[1], swimmerProvider.getSwimmer("sw1").getId());
  EXPECT_EQ(*relaySwimmers[2], swimmerProvider.getSwimmer("sw2").getId());
  EXPECT_EQ(*relaySwimmers[3], swimmerProvider.getSwimmer("sw3").getId());
}
