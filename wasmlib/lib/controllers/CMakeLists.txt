add_library(controllers OBJECT)
target_sources(controllers
    PUBLIC
      AutoFillRelayController.h
      BackupController.h
      RelayHelperController.h
      RelayOperationsController.h
      RelaySetSwimmerController.h
      RelaySummaryController.h
      SessionOperationsController.h
      SessionSummaryController.h
      SwimmerOperationsController.h
      SwimmerTimesController.h
      SwimmerToSessionController.h
      SwimmerToTeamController.h
      TeamLoadController.h
      TeamModificationsController.h
      TeamOperationsController.h
      TeamSummaryController.h
    PRIVATE
)
target_include_directories(controllers PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(controllers PUBLIC nlohmann_json::nlohmann_json utils models Microsoft.GSL::GSL)
set_target_properties(controllers PROPERTIES LINKER_LANGUAGE CXX)

if(DEFINED ADD_TESTS)
    add_subdirectory(tests)
endif()

add_subdirectory(local)
