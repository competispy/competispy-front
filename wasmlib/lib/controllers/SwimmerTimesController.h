#ifndef COMPETYSPY_FRONT_SWIMMER_TIMES_CONTROLLER_H
#define COMPETYSPY_FRONT_SWIMMER_TIMES_CONTROLLER_H

#include "Swimmer.h"

class SwimmerTimesController {
public:
  explicit SwimmerTimesController(Swimmer& swimmer) : swimmer_(swimmer) {}

  void setEventTime(const Event& event, float time, Times::Pool pool) {
    swimmer_.setEventTime(event, time, pool);
  }

private:
  Swimmer& swimmer_;
};

#endif
