#ifndef COMPETYSPY_FRONT_RELAY_SUMMARY_CONTROLLER_H
#define COMPETYSPY_FRONT_RELAY_SUMMARY_CONTROLLER_H

#include <cstdint>
#include <optional>
#include <vector>

#include "Relay.h"
#include "Style.h"
#include "Swimmer.h"

class RelaySummaryController {
public:
  struct SwimmerEntry {
    Swimmer::SwimmerId id;
    float              time;
    uint16_t           age;
  };

  class SwimmerEntryWrapper {
  public:
    SwimmerEntryWrapper(std::optional<SwimmerEntry> entry, Style style) :
        entry_(std::move(entry)),
        style_(style) {}

    SwimmerEntryWrapper() : SwimmerEntryWrapper(std::nullopt, Style::FREE) {}

    [[nodiscard]] auto available() const -> bool { return entry_.has_value(); }

    [[nodiscard]] auto id() const -> const Swimmer::SwimmerId& { return entry_->id; }

    [[nodiscard]] auto style() const -> Style { return style_; }

    [[nodiscard]] auto time() const -> float { return entry_->time; }

    [[nodiscard]] auto age() const -> uint16_t { return entry_->age; }

  private:
    std::optional<SwimmerEntry> entry_;
    Style                       style_;
  };

  struct RelaySummaryEntry {
    Relay::Id                        id{};
    std::vector<SwimmerEntryWrapper> swimmers{4};
    float                            totalTime = 0;
    uint16_t                         sumAge    = 0;
  };

  RelaySummaryController(const Relay& relay) : relay_(relay) {}

  auto getSummary(uint16_t year) {
    RelaySummaryEntry entry{
      relay_.getId(), {}, relay_.getAccumulatedTime(), relay_.getSumAge(year)};
    entry.swimmers.resize(4);
    relay_.visitSwimmers([this, &entry, year](const Swimmer* swimmer, size_t position) {
      const auto [event, _] = relay_.getPositionData(position);
      if (nullptr == swimmer) {
        entry.swimmers[position] = SwimmerEntryWrapper(std::nullopt, event.style);

      } else {
        SwimmerEntry swimmerEntry{swimmer->getId(),
                                  swimmer->getTimeAutoConvert(event, relay_.getPool()),
                                  swimmer->getAge(year)};
        entry.swimmers[position] = SwimmerEntryWrapper(std::move(swimmerEntry), event.style);
      }
    });
    return entry;
  }

protected:
  const Relay& relay_;
};

#endif
