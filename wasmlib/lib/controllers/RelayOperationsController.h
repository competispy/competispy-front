#ifndef COMPETYSPY_FRONT_RELAYOPERATIONSCONTROLLER_H
#define COMPETYSPY_FRONT_RELAYOPERATIONSCONTROLLER_H

#include "AutoFillRelayController.h"
#include "Relay.h"
#include "RelayObserver.h"

class RelayOperationsController {
  using RelaySwimmersEntry = std::tuple<Swimmer::SwimmerId, float, Event>;

public:
  explicit RelayOperationsController(Relay& relay, Session& session, uint16_t actualYear) :
      relay_(relay),
      session_(session),
      actualYear_(actualYear),
      autoFillRelayController_(relay_, session_, actualYear_) {}

  void               fill() { autoFillRelayController_.fill(); }

  [[nodiscard]] auto getSwimmersForPosition(size_t position) const {
    return autoFillRelayController_.getSwimmersForPosition(position);
  }

  void setSwimmer(const Swimmer::SwimmerId& swimmerId, size_t position) {
    session_.setSwimmerInRelayPosition(swimmerId, relay_.getId(), position);
  }

  void               removeSwimmer(size_t position) { relay_.removeSwimmer(position); }

  void               clean() { relay_.clean(); }

  [[nodiscard]] auto getAccumulatedTime() const -> float { return relay_.getAccumulatedTime(); }

  [[nodiscard]] auto getSumAge() const -> uint16_t { return relay_.getSumAge(actualYear_); }

  [[nodiscard]] auto getSwimmers() const -> std::vector<std::optional<RelaySwimmersEntry>> {
    std::vector<std::optional<RelaySwimmersEntry>> swimmers;
    swimmers.reserve(4);
    relay_.visitSwimmers([&swimmers, this](const Swimmer* swimmer, size_t position) {
      if (swimmer != nullptr) {
        Event event = std::get<0>(relay_.getPositionData(position));
        swimmers.emplace_back(RelaySwimmersEntry{
          swimmer->getId(), swimmer->getTimeAutoConvert(event, relay_.getPool()), event});
      } else {
        swimmers.emplace_back(std::nullopt);
      }
    });
    return swimmers;
  }

  void registerRelayChangeObserver(RelayObserver* relayObserver) {
    relay_.registerRelayObserver(relayObserver);
  }

  void unregisterRelayChangeObserver(RelayObserver* relayObserver) {
    relay_.unregisterRelayObserver(relayObserver);
  }

private:
  Relay&                  relay_;
  Session&                session_;
  const uint16_t          actualYear_;
  AutoFillRelayController autoFillRelayController_;
};

#endif  // COMPETYSPY_FRONT_RELAYOPERATIONSCONTROLLER_H
