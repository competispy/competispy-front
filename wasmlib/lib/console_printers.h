#ifndef COMPETYSPY_FRONT_CONSOLE_PRINTERS_H
#define COMPETYSPY_FRONT_CONSOLE_PRINTERS_H

#include <iostream>

#include "Event.h"
#include "RelayCategory.h"
#include "Style.h"
#include "StyleTimes.h"
#include "Times.h"

inline std::ostream& operator<<(std::ostream& os, const Times::Pool& value) {
  switch (value) {
    case Times::Pool::_25 :
      os << "25";
      break;
    case Times::Pool::_50 :
      os << "50";
      break;
  }
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const EventGender& value) {
  switch (value) {
    case EventGender::MALE :
      os << "male";
      break;
    case EventGender::FEMALE :
      os << "female";
      break;
    case EventGender::MIXED :
      os << "mixed";
      break;
  }
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const Style& value) {
  switch (value) {
    case Style::BUTTERFLY :
      os << "butterfly";
      break;
    case Style::BACK :
      os << "back";
      break;
    case Style::BREAST :
      os << "breast";
      break;
    case Style::FREE :
      os << "free";
      break;
    case Style::MEDLEY :
      os << "medley";
      break;
  }
  return os;
}

/*
inline std::ostream &operator<<(std::ostream &os, const Gender &value) {
  switch (value) {
    case Gender::MALE:
      os << "male";
      break;
    case Gender::FEMALE:
      os << "female";
      break;
  }
  return os;
}
*/

inline std::ostream& operator<<(std::ostream& os, const RelayCategory& value) {
  switch (value) {
    case RelayCategory::_80 :
      os << "80";
      break;
    case RelayCategory::_100 :
      os << "100";
      break;
    case RelayCategory::_120 :
      os << "120";
      break;
    case RelayCategory::_160 :
      os << "160";
      break;
    case RelayCategory::_200 :
      os << "200";
      break;
    case RelayCategory::_240 :
      os << "240";
      break;
    case RelayCategory::_280 :
      os << "280";
      break;
    case RelayCategory::_320 :
      os << "320";
      break;
    case RelayCategory::_360 :
      os << "360";
      break;
  }
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const Event& value) {
  os << value.distance << " " << value.style << " " << value.gender;
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const StyleTimes& value) {
  os << "style=" << value.getStyle() << ", ";
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const Times& value) {
  os << "25: ";
  return os;
}

#endif  // COMPETYSPY_FRONT_CONSOLE_PRINTERS_H
