#include <emscripten.h>
#include <emscripten/bind.h>

#include "Event.h"
#include "RelaySetSwimmerController.h"
#include "Swimmer.h"

struct AvailableSwimmerWrapper {
  Swimmer::SwimmerId id;
  Event              event;
  float              time;
};

auto getAvailableSwimmers(const RelaySetSwimmerController& controller) {
  const auto&                          swimmers = controller.getAvailableSwimmers();
  std::vector<AvailableSwimmerWrapper> availableSwimmers;
  availableSwimmers.reserve(swimmers.size());
  for (const auto& [id, event, time] : swimmers) {
    availableSwimmers.emplace_back(id, event, time);
  }
  return availableSwimmers;
}

using namespace emscripten;

EMSCRIPTEN_BINDINGS(RelaySetSwimmerBindings) {
  using namespace emscripten;

  class_<AvailableSwimmerWrapper>("AvailableSwimmerWrapper")
      .property("id", &AvailableSwimmerWrapper::id)
      .property("event", &AvailableSwimmerWrapper::event)
      .property("time", &AvailableSwimmerWrapper::time);

  register_vector<AvailableSwimmerWrapper>("VectorAvailableSwimmerWrapper");

  class_<RelaySetSwimmerController>("RelaySetSwimmerController")
      .function("getAvailableSwimmers", &getAvailableSwimmers)
      .function("removeSwimmer", &RelaySetSwimmerController::removeSwimmer)
      .function("setSwimmer", &RelaySetSwimmerController::setSwimmer);
}
