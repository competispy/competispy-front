#include <emscripten.h>

#include "Event.h"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(EventBindings) {
  using namespace emscripten;

  enum_<EventGender>("EventGender")
      .value("MALE", EventGender::MALE)
      .value("FEMALE", EventGender::FEMALE)
      .value("MIXED", EventGender::MIXED);

  value_object<Event>("Event")
      .field("style", &Event::style)
      .field("distance", &Event::distance)
      .field("gender", &Event::gender);
}
