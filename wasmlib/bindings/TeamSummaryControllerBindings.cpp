#include <emscripten.h>
#include <emscripten/bind.h>

#include "TeamSummaryController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(TeamSummaryControllerBinding) {
  using namespace emscripten;

  register_map<std::string, bool>("MapStringBool");
  register_vector<std::string>("vectorString");

  value_object<TeamSummaryController::EventPoolTime>("EventPoolTime")
      .field("event", &TeamSummaryController::EventPoolTime::event)
      .field("pool", &TeamSummaryController::EventPoolTime::pool)
      .field("time", &TeamSummaryController::EventPoolTime::time);

  value_object<TeamSummaryController::SummaryEntry>("TeamSummaryControllerEntry")
      .field("swimmerId", &TeamSummaryController::SummaryEntry::swimmerId)
      .field("sessionAvailability", &TeamSummaryController::SummaryEntry::sessionAvailability)
      .field("age", &TeamSummaryController::SummaryEntry::age)
      .field("fly50", &TeamSummaryController::SummaryEntry::fly50)
      .field("fly100", &TeamSummaryController::SummaryEntry::fly100)
      .field("back50", &TeamSummaryController::SummaryEntry::back50)
      .field("back100", &TeamSummaryController::SummaryEntry::back100)
      .field("breast50", &TeamSummaryController::SummaryEntry::breast50)
      .field("breast100", &TeamSummaryController::SummaryEntry::breast100)
      .field("free50", &TeamSummaryController::SummaryEntry::free50)
      .field("free100", &TeamSummaryController::SummaryEntry::free100);

  register_vector<TeamSummaryController::SummaryEntry>("VectorTeamSummaryController_SummaryEntry");

  class_<TeamSummaryController>("TeamSummaryController")
      .function("getSummary", &TeamSummaryController::getSummary);
}
