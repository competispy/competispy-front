#include <emscripten.h>

#include "Style.h"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(StyleBindings) {
  using namespace emscripten;

  enum_<Style>("Style")
      .value("BUTTERFLY", Style::BUTTERFLY)
      .value("BACK", Style::BACK)
      .value("BREAST", Style::BREAST)
      .value("FREE", Style::FREE)
      .value("MEDLEY", Style::MEDLEY);
}
