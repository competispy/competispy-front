#include <cstdint>
#include <emscripten.h>
#include <emscripten/bind.h>
#include <memory>
#include <string>
#include <utility>

#include "nlohmann/json_fwd.hpp"
#include "spdlog/common.h"
#include "spdlog/spdlog.h"

#include "Event.h"
#include "Relay.h"
#include "RelayHelperController.h"
#include "RelayHelperControllerLocal.h"
#include "RelayObserver.h"
#include "SessionOperationsController.h"
#include "Swimmer.h"
#include "Times.h"

auto getRelayHelperController() -> std::shared_ptr<RelayHelperController> {
#ifdef LOG_LEVEL_DEBUG
  spdlog::set_level(spdlog::level::debug);
#else
  spdlog::set_level(spdlog::level::info);
#endif
  static auto relayHelperControllerLocal = std::make_shared<RelayHelperControllerLocal>();
  // static RelayHelperControllerLocal relayHelperControllerLocal;
  return relayHelperControllerLocal;
}

auto getSessionOperationController(const std::shared_ptr<RelayHelperController>& relayHelper,
                                   const std::string& teamName, const std::string& sessionId,
                                   uint16_t year) -> SessionOperationsController {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  return relayHelper->getTeamOperationsController()
      .getTeamModificationController(teamName, year)
      .getSessionOperationController(sessionId);
}

void fillRelay(const std::shared_ptr<RelayHelperController>& controller,
               const std::string& teamName, const std::string& sessionId, const Relay::Id& relayId,
               uint16_t year) {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  getSessionOperationController(controller, teamName, sessionId, year)
      .getRelayOperationsController(relayId)
      .fill();
}

void cleanRelay(const std::shared_ptr<RelayHelperController>& controller,
                const std::string& teamName, const std::string& sessionId,
                const Relay::Id& relayId) {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  getSessionOperationController(controller, teamName, sessionId, 2000)
      .getRelayOperationsController(relayId)
      .clean();
}

auto isTeamLoaded(const std::shared_ptr<RelayHelperController>& controller,
                  const std::string&                            teamName) -> bool {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  return controller->getTeamOperationsController().getTeam(teamName).getSwimmersSize() != 0;
}

void setSwimmerTime(const std::shared_ptr<RelayHelperController>& controller,
                    const Swimmer::SwimmerId& swimmerId, const Event& event,
                    const Times::Pool& pool, float time) {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  controller->getSwimmerOperationsController().getSwimmerTimesController(swimmerId).setEventTime(
      event, time, pool);
}

class RelayObserverBindingImpl : public RelayObserver {
public:
  RelayObserverBindingImpl(std::shared_ptr<RelayHelperController>& relayHelper,
                           std::string teamName, std::string sessionId, const Relay::Id& relayId,
                           emscripten::val callback) :
      relayHelper_(relayHelper),
      teamName_(std::move(teamName)),
      sessionId_(std::move(sessionId)),
      relayId_(relayId),
      cb_(std::move(callback)) {}

  ~RelayObserverBindingImpl() override = default;

  void notifyChanged() override {
    spdlog::debug("[RelayObserverBindingImpl] calling callback");
    cb_();
  }

  void notifyRemoved() override { isRemoved_ = true; }

  void registerObserver() {
    spdlog::debug("[RelayObserverBindingImpl] registerObserver {}", relayId_);
    relayHelper_->registerRelayChangedObserver(teamName_, sessionId_, relayId_, this);
  }

  void unregisterObserver() {
    spdlog::debug("[RelayObserverBindingImpl] unregisterObserver {}", relayId_);
    if (not isRemoved_) {
      relayHelper_->unregisterRelayChangedObserver(teamName_, sessionId_, relayId_, this);
    }
  }

private:
  std::shared_ptr<RelayHelperController> relayHelper_;
  std::string                            teamName_;
  std::string                            sessionId_;
  Relay::Id                              relayId_;
  emscripten::val                        cb_;
  bool                                   isRemoved_ = false;
};

auto createRelayObserver(std::shared_ptr<RelayHelperController>& controller, std::string teamName,
                         std::string sessionId, const Relay::Id& relayId, emscripten::val callback)
    -> std::unique_ptr<RelayObserverBindingImpl> {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  return std::make_unique<RelayObserverBindingImpl>(
      controller, std::move(teamName), std::move(sessionId), relayId, std::move(callback));
}

auto createBackup(std::shared_ptr<RelayHelperController>& controller) -> std::string {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  return static_cast<nlohmann::json>(*static_cast<RelayHelperControllerLocal*>(controller.get()))
      .dump(2);
}

void loadBackup(std::shared_ptr<RelayHelperController>& controller, const std::string& backup) {
  spdlog::debug("[RelayHelperController] {}", __FUNCTION__);
  static_cast<RelayHelperControllerLocal*>(controller.get())
      ->loadBackup(nlohmann::json::parse(backup));
}

using namespace emscripten;

EMSCRIPTEN_BINDINGS(RelayHelperControllerBindings) {
  class_<RelayObserverBindingImpl>("RelayObserverBindingImpl")
      .function("registerObserver", &RelayObserverBindingImpl::registerObserver)
      .function("unregisterObserver", &RelayObserverBindingImpl::unregisterObserver);
  class_<RelayHelperController>("RelayHelperController")
      /*
        .function("getSwimmerToSessionController",
                  &RelayHelperControllerLocal::getSwimmerToSessionController)
      */
      .function("getTeamLoadController", &RelayHelperController::getTeamLoadController)
      .function("getTeamSummaryController", &RelayHelperController::getTeamSummaryController)
      .function("getSessionsSummaryController",
                &RelayHelperController::getSessionsSummaryController)
      .function("getRelaySetSwimmerController",
                &RelayHelperController::getRelaySetSwimmerController)
      .function("getSessionOperationController", &getSessionOperationController)
      .function("getSwimmerToSessionController",
                &RelayHelperController::getSwimmerToSessionController)
      .function("getAvailableSessionIds", &RelayHelperController::getAvailableSessionIds)
      .function("addNewSession", &RelayHelperController::addNewSession)
      .function("removeSession", &RelayHelperController::removeSession)
      .function("fillRelay", &fillRelay)
      .function("cleanRelay", &cleanRelay)
      .function("setSwimmerTime", &setSwimmerTime)
      .function("createRelayObserver", &createRelayObserver)
      .function("createBackup", &createBackup)
      .function("loadBackup", &loadBackup)
      .function("isTeamLoaded", &isTeamLoaded)
      .class_function("getInstance", &getRelayHelperController)
      .smart_ptr<std::shared_ptr<RelayHelperController>>("RelayHelperController");
}
