#include <emscripten.h>
#include <emscripten/bind.h>

#include "SwimmerTimesController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(SwimmerTimesControllerBinding) {
  using namespace emscripten;

  class_<SwimmerTimesController>("SwimmerTimesController")
      .function("setEventTime", &SwimmerTimesController::setEventTime);
}
