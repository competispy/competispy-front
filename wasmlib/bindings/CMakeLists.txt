option(LOG_LEVEL_DEBUG "Set log level to debug" OFF)

set(CMAKE_EXECUTABLE_SUFFIX ".mjs")

add_executable(TeamsRelays
        EventBindings.cpp
        GenderBindings.cpp
        RelayBindings.cpp
        RelayCategoryBindings.cpp
        RelayHelperControllerBindings.cpp
        RelaySetSwimmerControllerBindings.cpp
        RelaySummaryControllerBindings.cpp
        SessionSummaryControllerBindings.cpp
        SessionOperationControllerBindings.cpp
        StyleBindings.cpp
        StyleTimesBindings.cpp
        SwimmerBindings.cpp
        SwimmerTimesControllerBindings.cpp
        SwimmerToSessionControllerBindings.cpp
        TeamLoadControllerBindings.cpp
        TeamSummaryControllerBindings.cpp
        TimesBindings.cpp
        )
target_link_libraries(TeamsRelays PUBLIC local_controllers local_models embind)
target_link_options(TeamsRelays PUBLIC -SWASM=1 -SENVIRONMENT=web -SUSE_ES6_IMPORT_META=0 -sNO_DISABLE_EXCEPTION_CATCHING --embind-emit-tsd ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/TeamsRelaysHelper.d.ts --bind)
if(LOG_LEVEL_DEBUG)
  target_compile_definitions(TeamsRelays PRIVATE LOG_LEVEL_DEBUG)
endif()

