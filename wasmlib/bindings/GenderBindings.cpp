#include <emscripten.h>

#include "Gender.h"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(GenderBindings) {
  using namespace emscripten;

  enum_<Gender>("Gender").value("MALE", Gender::MALE).value("FEMALE", Gender::FEMALE);
}
