#include <emscripten.h>
#include <emscripten/bind.h>

#include "RelaySummaryController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(RelaySummaryControllerBinding) {
  using namespace emscripten;

  class_<RelaySummaryController::SwimmerEntryWrapper>("SwimmerEntryWrapper")
      .function("available", &RelaySummaryController::SwimmerEntryWrapper::available)
      .function("id", &RelaySummaryController::SwimmerEntryWrapper::id)
      .function("style", &RelaySummaryController::SwimmerEntryWrapper::style)
      .function("time", &RelaySummaryController::SwimmerEntryWrapper::time)
      .function("age", &RelaySummaryController::SwimmerEntryWrapper::age);

  value_object<RelaySummaryController::RelaySummaryEntry>("RelaySummaryEntry")
      .field("id", &RelaySummaryController::RelaySummaryEntry::id)
      .field("swimmers", &RelaySummaryController::RelaySummaryEntry::swimmers)
      .field("totalTime", &RelaySummaryController::RelaySummaryEntry::totalTime)
      .field("sumAge", &RelaySummaryController::RelaySummaryEntry::sumAge);

  register_vector<RelaySummaryController::SwimmerEntryWrapper>(
      "VectorRelaySummaryController_SwimmerEntryWrapper");

  class_<RelaySummaryController>("RelaySummaryController")
      .function("getSummary", &RelaySummaryController::getSummary);
}
